#include "grid.h"
#include "game/game.h"

namespace match3
{
	namespace grid
	{
		locker::Locker* locker[gridRow][gridCol];
		Rectangle rec;
		Texture2D background;

		void init()
		{
			rec = {
				static_cast<float>(game::screenWidth / 4), static_cast<float>(game::screenHeight / 2),
				static_cast<float>(game::screenWidth / 2), static_cast<float>(game::screenHeight / 2) };

			background = texture::gridBackground;
			background.width = static_cast<int>(rec.width);
			background.height = static_cast<int>(rec.height);

			locker::init();

			if (!locker::checkLockerAvaibles())
			{
				locker::reset();
			}
		}

		void update()
		{
			gameplay::player->clickOnFirstLocker();
			gameplay::player->selectLocker();
			gameplay::player->matchLocker();

			locker::update();
		}

		void draw()
		{
			DrawTextureEx(background, { rec.x, rec.y }, 0.0f, 1.0f, WHITE);

			locker::draw();
		}

		void deInit()
		{
			locker::deInit();
		}
	}
}