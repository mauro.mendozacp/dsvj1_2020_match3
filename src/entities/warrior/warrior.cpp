#include "warrior.h"
#include "audio/audio.h"
#include "texture/texture.h"
#include "scenes/gameplay/gameplay.h"

namespace match3
{
	namespace warrior
	{
		Rectangle enemyWarriorRec;

		void init()
		{
			enemyWarriorRec.width = warzone::rec.width / 12;
			enemyWarriorRec.height = warzone::rec.height / 4;
			enemyWarriorRec.x = warzone::enemyStartPositionX;
			enemyWarriorRec.y = warzone::floorY - enemyWarriorRec.height;
		}

		void add()
		{
			float auxDestX = gameplay::player->getRec().x + gameplay::player->getRec().width;

			warzone::enemies.push_back(new Warrior(
				enemyWarriorRec, ENEMY_WARRIOR_LIFE, ENEMY_WARRIOR_DAMAGE, ENEMY_WARRIOR_MOVE_SPEED, 
				ENEMY_WARRIOR_ATTACK_SPEED, auxDestX, ENEMY_WARRIOR_POINTS, audio::swordSound));
		}

		Warrior::Warrior() : Enemy()
		{
		}

		Warrior::Warrior(Rectangle rec, int life, int damage, float moveSpeed, float attackSpeed, float destinyX, int points, Sound attackSound)
			: Enemy(rec, life, damage, moveSpeed, attackSpeed, destinyX, points, attackSound)
		{
			setAnimation(character::ANIMATION_STATE::WALK);
		}

		Warrior::~Warrior()
		{
		}

		void Warrior::setAnimation(character::ANIMATION_STATE animationState)
		{
			Texture2D auxTexture = Texture2D();
			Rectangle auxPlayerRec = { 0.0f, 0.0f, this->rec.width, this->rec.height };
			int auxFramesX = 0;
			int auxFramesY = 0;
			float auxSpeed = 0.0f;
			bool auxIsLoop = false;

			switch (animationState)
			{
			case character::ANIMATION_STATE::IDLE:
				auxFramesX = 6;
				auxFramesY = 2;

				rec.x = destinyX;
				auxTexture = texture::enemyWarriorIdle;
				auxPlayerRec.width = enemyWarriorRec.width;
				auxPlayerRec.height = enemyWarriorRec.height;

				auxSpeed = 600.0f;
				auxIsLoop = true;

				break;
			case character::ANIMATION_STATE::WALK:
				auxFramesX = 6;
				auxFramesY = 3;

				auxTexture = texture::enemyWarriorWalk;
				auxPlayerRec.width = enemyWarriorRec.width;
				auxPlayerRec.height = enemyWarriorRec.height;

				auxSpeed = 600.0f;
				auxIsLoop = true;

				break;
			case character::ANIMATION_STATE::ATTACK:
				auxFramesX = 6;
				auxFramesY = 2;

				rec.x = destinyX - (auxPlayerRec.width * 25 / 100);

				auxTexture = texture::enemyWarriorAttack;
				auxPlayerRec.width = enemyWarriorRec.width * 125 / 100;
				auxPlayerRec.height = enemyWarriorRec.height;

				auxSpeed = 1200.0f;
				auxIsLoop = false;

				break;
			case character::ANIMATION_STATE::HURT:
				auxFramesX = 6;
				auxFramesY = 2;

				auxTexture = texture::enemyWarriorHurt;
				auxPlayerRec.width = enemyWarriorRec.width;
				auxPlayerRec.height = enemyWarriorRec.height;

				auxSpeed = 1200.0f;
				auxIsLoop = false;

				break;
			case character::ANIMATION_STATE::DEAD:
				auxFramesX = 6;
				auxFramesY = 3;

				auxTexture = texture::enemyWarriorDead;
				auxPlayerRec.width = enemyWarriorRec.width * 125 / 100;
				auxPlayerRec.height = enemyWarriorRec.height;

				auxSpeed = 900.0f;
				auxIsLoop = false;

				break;
			default:
				break;
			}

			auxTexture.width = static_cast<int>(auxPlayerRec.width * auxFramesX);
			auxTexture.height = static_cast<int>(auxPlayerRec.height * auxFramesY);

			this->animationState = animationState;
			this->animation = animation::Animation(auxTexture, auxPlayerRec, (auxFramesX * auxFramesY), auxSpeed, auxIsLoop);
		}
	}
}