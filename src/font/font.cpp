#include "font.h"

namespace match3
{
	namespace font
	{
		Font title;
		Font option;

		float titleFontSize = 45;
		float optionFontSize = 25;
		float valueFontSize = 20;

		float titleSpacing = 10.0f;
		float optionSpacing = 5.0f;
		float valueSpacing = 3.0f;

		Color titleColor = WHITE;
		Color optionColorSelected = YELLOW;
		Color optionColorNoSelected = WHITE;

		void init()
		{
			option = LoadFont("res/assets/fonts/option.ttf");
			title = LoadFont("res/assets/fonts/title.ttf");
		}

		void deInit()
		{
			UnloadFont(option);
			UnloadFont(title);
		}
	}
}