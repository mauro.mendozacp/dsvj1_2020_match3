#ifndef INPUT_H
#define INPUT_H

#include "raylib.h"

namespace match3
{
	namespace input
	{
		bool checkMouseOnRec(Rectangle rec);
		bool checkClickOnRec(Rectangle rec);
		bool checkMouseDownOnRec(Rectangle rec);
		bool checkMouseReleased();
		void mousePositionUpdate();
	}
}

#endif // !INPUT_H