#ifndef OPTION_H
#define OPTION_H

#include "raylib.h"
#include <iostream>

namespace match3
{
	namespace option
	{
		extern float titleRecWidth;
		extern float titleRecHeight;

		extern float optionRecWidth;
		extern float optionRecHeight;

		extern float highscoreRecWidth;
		extern float highscoreRecHeight;

		class Option
		{
		public:
			Option();
			Option(Vector2 position, Rectangle rec, Texture2D texture, std::string text, Font font, float fontSize, float spacing, Color color);
			~Option();

			void show();
			void changeOptionState();

			void setRec(Rectangle rec);
			Rectangle getRec();
			void setPosition(Vector2 position);
			Vector2 getPosition();
			Rectangle getRecPosition();
			void setTexture(Texture2D texture);
			Texture2D getTexture();
			void setText(std::string text);
			std::string getText();
			void setFont(Font font);
			Font getFont();
			void setFontSize(float fontSize);
			float setFontSize();
			void setSpacing(float spacing);
			float getSpacing();
			void setColor(Color color);
			Color getColor();

		private:
			Vector2 position;
			Rectangle rec;
			Texture2D texture;
			std::string text;
			Font font;
			float fontSize;
			float spacing;
			Color color;
		};

		void pauseInit();
		void pauseDeInit();

		void highscoreInit(Option*& highscore, std::string titleText);
		void highscoreDeInit(Option*& highscore);

		void init(Option* &title, std::string titleText, Option* options[], int optionLenght, std::string optionTexts[]);
		int getOption(Option* options[], int optionLenght);
		void update(Option* options[], int optionLenght);
		void draw(Option* title, Option* options[], int optionLenght);
		void deInit(Option* &title, Option* options[], int optionLenght);
	}
}

#endif // !OPTION_H