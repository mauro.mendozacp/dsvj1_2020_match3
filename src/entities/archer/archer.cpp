#include "archer.h"
#include "audio/audio.h"
#include "texture/texture.h"
#include "scenes/gameplay/gameplay.h"

namespace match3
{
	namespace archer
	{
		Rectangle enemyArcherRec;
		int destXMin;
		int destXMax;

		void init()
		{
			enemyArcherRec.width = warzone::rec.width / 12;
			enemyArcherRec.height = warzone::rec.height / 4;
			enemyArcherRec.x = warzone::enemyStartPositionX;
			enemyArcherRec.y = warzone::floorY - enemyArcherRec.height;

			destXMin = static_cast<int>(warzone::rec.width / 2);
			destXMax = static_cast<int>(warzone::rec.width * 3 / 4);
		}

		void add()
		{
			float auxDestX = static_cast<float>(GetRandomValue(destXMin, destXMax));

			warzone::enemies.push_back(new Archer(
				enemyArcherRec, ENEMY_ARCHER_LIFE, ENEMY_ARCHER_DAMAGE, ENEMY_ARCHER_MOVE_SPEED, 
				ENEMY_ARCHER_ATTACK_SPEED, auxDestX, ENEMY_ARCHER_POINTS, audio::arrowSound));
		}

		Archer::Archer() : Enemy()
		{
		}

		Archer::Archer(Rectangle rec, int life, int damage, float moveSpeed, float attackSpeed, float destinyX, int points, Sound attackSound)
			: Enemy(rec, life, damage, moveSpeed, attackSpeed, destinyX, points, attackSound)
		{
			setAnimation(character::ANIMATION_STATE::WALK);
		}

		Archer::~Archer()
		{
		}

		void Archer::setAnimation(character::ANIMATION_STATE animationState)
		{
			Texture2D auxTexture = Texture2D();
			Rectangle auxPlayerRec = { 0.0f, 0.0f, this->rec.width, this->rec.height };
			int auxFramesX = 0;
			int auxFramesY = 0;
			float auxSpeed = 0.0f;
			bool auxIsLoop = false;

			switch (animationState)
			{
			case character::ANIMATION_STATE::IDLE:
				auxFramesX = 6;
				auxFramesY = 2;

				auxTexture = texture::enemyArcherIdle;
				auxPlayerRec.width = enemyArcherRec.width;
				auxPlayerRec.height = enemyArcherRec.height;

				auxSpeed = 600.0f;
				auxIsLoop = true;

				break;
			case character::ANIMATION_STATE::WALK:
				auxFramesX = 6;
				auxFramesY = 3;

				auxTexture = texture::enemyArcherWalk;
				auxPlayerRec.width = enemyArcherRec.width;
				auxPlayerRec.height = enemyArcherRec.height;

				auxSpeed = 600.0f;
				auxIsLoop = true;

				break;
			case character::ANIMATION_STATE::ATTACK:
				auxFramesX = 6;
				auxFramesY = 3;

				auxTexture = texture::enemyArcherAttack;
				auxPlayerRec.width = enemyArcherRec.width * 110 / 100;
				auxPlayerRec.height = enemyArcherRec.height;

				auxSpeed = 1200.0f;
				auxIsLoop = false;

				break;
			case character::ANIMATION_STATE::HURT:
				auxFramesX = 6;
				auxFramesY = 2;

				auxTexture = texture::enemyArcherHurt;
				auxPlayerRec.width = enemyArcherRec.width;
				auxPlayerRec.height = enemyArcherRec.height;

				auxSpeed = 1200.0f;
				auxIsLoop = false;

				break;
			case character::ANIMATION_STATE::DEAD:
				auxFramesX = 6;
				auxFramesY = 3;

				auxTexture = texture::enemyArcherDead;
				auxPlayerRec.width = enemyArcherRec.width * 125 / 100;
				auxPlayerRec.height = enemyArcherRec.height;

				auxSpeed = 900.0f;
				auxIsLoop = false;

				break;
			default:
				break;
			}

			auxTexture.width = static_cast<int>(auxPlayerRec.width * auxFramesX);
			auxTexture.height = static_cast<int>(auxPlayerRec.height * auxFramesY);

			this->animationState = animationState;
			this->animation = animation::Animation(auxTexture, auxPlayerRec, (auxFramesX * auxFramesY), auxSpeed, auxIsLoop);
		}
	}
}