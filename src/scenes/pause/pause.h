#ifndef PAUSE_H
#define PAUSE_H

#include "raylib.h"

namespace match3
{
	namespace pause
	{
		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif // !PAUSE_H