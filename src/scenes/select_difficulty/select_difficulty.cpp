#include "select_difficulty.h"
#include "game/game.h"

namespace match3
{
	namespace select_difficulty
	{
		enum class SELECT_DIFFICULTY_MENU
		{
			EASY = 1,
			MEDIUM,
			HARD,
			BACK
		};

		const int optionLenght = 4;
		option::Option* options[optionLenght];
		std::string optionTexts[optionLenght] = { "EASY", "MEDIUM", "HARD", "BACK" };

		static void acceptOption(SELECT_DIFFICULTY_MENU option)
		{
			switch (option)
			{
			case SELECT_DIFFICULTY_MENU::EASY:
				gameplay::difficulty = gameplay::DIFFICULTY::EASY;
				break;
			case SELECT_DIFFICULTY_MENU::MEDIUM:
				gameplay::difficulty = gameplay::DIFFICULTY::MEDIUM;
				break;
			case SELECT_DIFFICULTY_MENU::HARD:
				gameplay::difficulty = gameplay::DIFFICULTY::HARD;
				break;
			default:
				break;
			}

			switch (option)
			{
			case SELECT_DIFFICULTY_MENU::EASY:
			case SELECT_DIFFICULTY_MENU::MEDIUM:
			case SELECT_DIFFICULTY_MENU::HARD:
				StopMusicStream(audio::menuMusic);
				game::changeStatus(game::GAME_STATUS::GAMEPLAY);
				break;
			case SELECT_DIFFICULTY_MENU::BACK:
				game::changeStatus(game::GAME_STATUS::MAIN_MENU);
				break;
			default:
				break;
			}
		}

		void init()
		{
			Vector2 pos = { static_cast<float>(game::screenWidth / 2) - (option::optionRecWidth / 2 / 2),
				static_cast<float>(game::screenHeight * 9 / 16) - (option::optionRecHeight / 2) };

			Rectangle bgTexture = { 0, 0, option::optionRecWidth / 2, option::optionRecHeight };

			Texture2D texture = texture::optionButton;
			texture.width = static_cast<int>(bgTexture.width);
			texture.height = static_cast<int>(option::optionRecHeight * 2);

			for (int i = 0; i < optionLenght; i++)
			{
				options[i] = new option::Option(pos, bgTexture, texture, optionTexts[i], font::option,
					font::optionFontSize, font::optionSpacing, font::optionColorNoSelected);

				pos.y += option::optionRecHeight * 3 / 2;
			}

		}

		void update()
		{
			for (int i = 0; i < optionLenght; i++)
			{
				options[i]->changeOptionState();
			}

			acceptOption(static_cast<SELECT_DIFFICULTY_MENU>(option::getOption(options, optionLenght)));
		}

		void draw()
		{
			for (int i = 0; i < optionLenght; i++)
			{
				options[i]->show();
			}
		}

		void deInit()
		{
			for (int i = 0; i < optionLenght; i++)
			{
				if (options[i] != NULL)
				{
					delete options[i];
					options[i] = NULL;
				}
			}
		}
	}
}