#include "animation.h"
#include "game/game.h"

namespace match3
{
	namespace animation
	{
		Animation::Animation()
		{
			this->texture = Texture2D();
			this->frameRec = Rectangle();
			this->frames = 0;
			this->currentFrame = 0;
			this->frameCounter = 0.0f;
			this->speed = 0;
			this->isLoop = false;
			this->isFinish = false;
		}

		Animation::Animation(Texture2D texture, Rectangle framRec, int frames, float speed, bool isLoop)
		{
			this->texture = texture;
			this->frameRec = framRec;
			this->frames = frames;
			this->currentFrame = 0;
			this->frameCounter = 0.0f;
			this->speed = speed;
			this->isLoop = isLoop;
			this->isFinish = false;
		}

		Animation::~Animation()
		{
		}

		void Animation::update()
		{
			if (!isFinish)
			{
				frameCounter += gameplay::frameTime;

				if (frameCounter >= (game::frames / speed))
				{
					frameCounter = 0.0f;
					currentFrame++;

					if (currentFrame > (frames - 1))
					{
						currentFrame = 0;
					}

					frameRec.x += frameRec.width;

					if (frameRec.x >= texture.width)
					{
						frameRec.x = 0.0f;
						frameRec.y += frameRec.height;

						if (frameRec.y >= texture.height)
						{
							frameRec.y = 0.0f;

							if (!isLoop)
							{
								frameRec.x = texture.width - frameRec.width;
								frameRec.y = texture.height - frameRec.height;
								isFinish = true;
							}
						}
					}
				}
			}
		}

		void Animation::draw(Vector2 position, Color color)
		{
			DrawTextureRec(texture, frameRec, position, color);
		}

		void Animation::setTexture(Texture2D texture)
		{
			this->texture = texture;
		}

		Texture2D Animation::getTexture()
		{
			return this->texture;
		}

		void Animation::setFrameRec(Rectangle frameRec)
		{
			this->frameRec = frameRec;
		}

		Rectangle Animation::getFrameRec()
		{
			return this->frameRec;
		}

		void Animation::setFrames(int frames)
		{
			this->frames = frames;
		}

		int Animation::getFrames()
		{
			return this->frames;
		}

		void Animation::setCurrentFrame(int currentFrame)
		{
			this->currentFrame = currentFrame;
		}

		int Animation::getCurrentFrame()
		{
			return this->currentFrame;
		}

		void Animation::setFrameCounter(float frameCounter)
		{
			this->frameCounter = frameCounter;
		}

		float Animation::getFrameCounter()
		{
			return this->frameCounter;
		}

		void Animation::setSpeed(float speed)
		{
			this->speed = speed;
		}

		float Animation::getSpeed()
		{
			return this->speed;
		}

		void Animation::setIsLoop(bool isLoop)
		{
			this->isLoop = isLoop;
		}

		bool Animation::getIsLoop()
		{
			return this->isLoop;
		}

		void Animation::setIsFinish(bool isFinish)
		{
			this->isFinish = isFinish;
		}

		bool Animation::getIsFinish()
		{
			return this->isFinish;
		}
	}
}