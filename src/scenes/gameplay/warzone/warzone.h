#ifndef WARZONE_H
#define WARZONE_H

#include <vector>
#include "raylib.h"
#include "entities/enemy/enemy.h"
#include "entities/parallax/parallax.h"

namespace match3
{
	namespace warzone
	{
		const int cloudsParallaxLenght = 3;

		extern Rectangle rec;
		extern Color color;
		extern Texture2D background;
		extern Texture2D floorTexture;
		extern parallax::Parallax* clouds;
		extern float floorY;
		extern float playerStartPositionX;
		extern float enemyStartPositionX;
		extern animation::Animation countdown;

		extern std::vector<enemy::Enemy*> enemies;

		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif // !WARZONE_H