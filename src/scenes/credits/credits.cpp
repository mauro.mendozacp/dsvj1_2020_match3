#include "credits.h"
#include "game/game.h"
#include "entities/option/option.h"

namespace match3
{
	namespace credits
	{
		option::Option* back;

		static void acceptOption()
		{
			if (input::checkClickOnRec(back->getRecPosition()))
			{
				game::changeStatus(game::GAME_STATUS::MAIN_MENU);
			}
		}

		void init()
		{
			//Back
			Vector2 pos = { static_cast<float>(game::screenWidth / 2) - (option::optionRecWidth / 2),
				static_cast<float>(game::screenHeight * 13 / 16) - (option::optionRecHeight / 2) };

			Rectangle bgTexture = { 0, 0, option::optionRecWidth, option::optionRecHeight };

			Texture2D texture = texture::optionButton;
			texture.width = static_cast<int>(option::optionRecWidth);
			texture.height = static_cast<int>(option::optionRecHeight * 2);

			back = new option::Option(pos, bgTexture, texture, "BACK", font::option, font::optionFontSize, font::optionSpacing, font::optionColorNoSelected);
		}

		void update()
		{
			back->changeOptionState();
			acceptOption();
		}

		void draw()
		{
			back->show();
		}

		void deInit()
		{
			if (back != NULL)
			{
				delete back;
			}
		}
	}
}