#ifndef LOCKER_INDEX_H
#define LOCKER_INDEX_H

namespace match3
{
	const int minSelectLocker = 3;
	struct LockerIndex
	{
		int i;
		int j;
	};
}

#endif // !LOCKER_INDEX_H