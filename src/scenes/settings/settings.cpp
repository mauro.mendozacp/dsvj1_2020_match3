#include "settings.h"
#include <iostream>
#include "game/game.h"
#include "entities/option/option.h"

namespace match3
{
	namespace settings
	{
		enum class SETTINGS_MENU
		{
			RESOLUTION = 1,
			MUSIC,
			SFX,
			GO_BACK
		};

		const int optionLenght = 4;
		option::Option* title;
		option::Option* options[optionLenght];

		std::string titleText = "SETTINGS";
		std::string optionTexts[optionLenght] = { "RESOLUTION", "MUSIC", "SFX", "BACK TO MENU" };

		const int resolutionLenght = 3;
		Vector2 resolutionValues[resolutionLenght] = { { 600, 400 }, { 840, 750 }, { 1240, 900 } };
		std::string resolutionsText[resolutionLenght] = { "RESOL. 600x400", "RESOL. 840x750", "RESOL. 1240x900" };
		float fontTitles[resolutionLenght] = { 35, 55, 75 };
		float fontOptions[resolutionLenght] = { 15, 25, 35 };
		Vector2 titleRecSize[resolutionLenght] = { { 400.0f, 100.0f }, { 600.0f, 150.0f }, { 800.0f, 200.0f } };
		Vector2 optionRecSize[resolutionLenght] = { { 200.0f, 30.0f }, { 300.0f, 50.0f }, { 400.0f, 75.0f } };
		Vector2 highscoreRecSize[resolutionLenght] = { { 350.0f, 40.0f }, { 450.0f, 70.0f }, { 600.0f, 80.0f } };

		const int volumeLenght = 2;
		float musicValue[volumeLenght] = { 0.0f, audio::musicDefaultVolume };
		float sfxValue[volumeLenght] = { 0.0f, audio::sfxDefaultVolume };
		std::string musicText[volumeLenght] = { "MUSIC OFF", "MUSIC ON" };
		std::string sfxText[volumeLenght] = { "SFX OFF", "SFX ON" };

		int valueIndexes[optionLenght - 1] = { 0, 0, 0 };

		static int getNextValue(int index, int lenght)
		{
			index++;

			if (index < lenght)
			{
				return index;
			}

			return 0;
		}

		static void setTexts()
		{
			for (int i = 0; i < resolutionLenght; i++)
			{
				if (game::screenWidth == resolutionValues[i].x && game::screenHeight == resolutionValues[i].y)
				{
					valueIndexes[0] = i;
					optionTexts[0] = resolutionsText[i];
					break;
				}
			}

			if (audio::musicVolume == 0.0f)
			{
				valueIndexes[1] = 0;
				optionTexts[1] = musicText[0];
			}
			else
			{
				valueIndexes[1] = 1;
				optionTexts[1] = musicText[1];
			}

			if (audio::sfxVolume == 0.0f)
			{
				valueIndexes[2] = 0;
				optionTexts[2] = sfxText[0];
			}
			else
			{
				valueIndexes[2] = 1;
				optionTexts[2] = sfxText[1];
			}
		}

		static void acceptOption(SETTINGS_MENU option)
		{
			int index = 0;

			switch (option)
			{
			case SETTINGS_MENU::RESOLUTION:
				index = getNextValue(valueIndexes[0], resolutionLenght);
				game::screenWidth = static_cast<int>(resolutionValues[index].x);
				game::screenHeight = static_cast<int>(resolutionValues[index].y);
				font::titleFontSize = fontTitles[index];
				font::optionFontSize = fontOptions[index];
				option::titleRecWidth = titleRecSize[index].x;
				option::titleRecHeight = titleRecSize[index].y;
				option::optionRecWidth = optionRecSize[index].x;
				option::optionRecHeight = optionRecSize[index].y;
				option::highscoreRecWidth = highscoreRecSize[index].x;
				option::highscoreRecHeight = highscoreRecSize[index].y;
				texture::deInit();
				texture::init();
				game::changeStatus(game::GAME_STATUS::SETTINGS);
				SetWindowSize(game::screenWidth, game::screenHeight);

				break;
			case SETTINGS_MENU::MUSIC:
				index = getNextValue(valueIndexes[1], volumeLenght);
				audio::setMusicVolume(musicValue[index]);

				break;
			case SETTINGS_MENU::SFX:
				index = getNextValue(valueIndexes[2], volumeLenght);
				audio::setSfxVolume(sfxValue[index]);

				break;
			case SETTINGS_MENU::GO_BACK:
				game::changeStatus(game::GAME_STATUS::MAIN_MENU);
				break;
			default:
				break;
			}

			if (static_cast<int>(option) > 0 && static_cast<int>(option) < 4)
			{
				setTexts();
				options[0]->setText(optionTexts[0]);
				options[1]->setText(optionTexts[1]);
				options[2]->setText(optionTexts[2]);
			}
		}

		void init()
		{
			setTexts();
			option::init(title, titleText, options, optionLenght, optionTexts);
		}

		void update()
		{
			option::update(options, optionLenght);
			acceptOption(static_cast<SETTINGS_MENU>(option::getOption(options, optionLenght)));
		}

		void draw()
		{
			option::draw(title, options, optionLenght);
		}

		void deInit()
		{
			option::deInit(title, options, optionLenght);
		}
	}
}