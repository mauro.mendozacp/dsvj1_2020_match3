#ifndef AUDIO_H
#define AUDIO_H

#include "raylib.h"

namespace match3
{
	namespace audio
	{
		extern Music gameplayMusic;
		extern Music menuMusic;

		extern Sound selectSound;
		extern Sound hitPlayerSound;
		extern Sound swordSound;
		extern Sound arrowSound;
		extern Sound fireBallSound;
		extern Sound iceBallSound;
		extern Sound thunderSound;

		extern const float musicDefaultVolume;
		extern const float sfxDefaultVolume;

		extern float musicVolume;
		extern float sfxVolume;

		void init();
		void deInit();

		void setMusicVolume(float volume);
		void setSfxVolume(float volume);
	}
}

#endif // !AUDIO_H