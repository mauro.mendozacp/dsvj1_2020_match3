#include "main_menu.h"
#include "game/game.h"
#include "entities/option/option.h"

namespace match3
{
	namespace main_menu
	{
		enum class MAIN_MENU
		{
			PLAY = 1,
			SETTINGS,
			CREDITS,
			EXIT
		};

		const int optionLenght = 4;
		option::Option* title;
		option::Option* options[optionLenght];
		option::Option* highscore;

		std::string titleText = "MATCH 3";
		std::string optionTexts[optionLenght] = { "PLAY", "SETTINGS", "CREDITS", "EXIT" };

		static void acceptOption(MAIN_MENU option)
		{
			switch (option)
			{
			case MAIN_MENU::PLAY:
				game::changeStatus(game::GAME_STATUS::SELECT_DIFFICULTY);
				break;
			case MAIN_MENU::SETTINGS:
				game::changeStatus(game::GAME_STATUS::SETTINGS);
				break;
			case MAIN_MENU::CREDITS:
				game::changeStatus(game::GAME_STATUS::CREDITS);
				break;
			case MAIN_MENU::EXIT:
				game::changeStatus(game::GAME_STATUS::EXIT);
				break;
			default:
				break;
			}
		}

		void init()
		{
			option::init(title, titleText, options, optionLenght, optionTexts);

			if (game::highScore > 0)
			{
				option::highscoreInit(highscore, TextFormat("HIGHSCORE: %i", game::highScore));
			}
			else
			{
				highscore = NULL;
			}
		}

		void update()
		{
			option::update(options, optionLenght);
			acceptOption(static_cast<MAIN_MENU>(option::getOption(options, optionLenght)));

			if (!IsMusicPlaying(audio::menuMusic))
			{
				PlayMusicStream(audio::menuMusic);
			}
		}

		void draw()
		{
			option::draw(title, options, optionLenght);

			if (highscore != NULL)
			{
				highscore->show();
			}
		}

		void deInit()
		{
			option::deInit(title, options, optionLenght);
			option::highscoreDeInit(highscore);
		}
	}
}