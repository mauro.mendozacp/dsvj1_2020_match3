#include "audio.h"

namespace match3
{
	namespace audio
	{
		Music gameplayMusic;
		Music menuMusic;

		Sound selectSound;
		Sound hitPlayerSound;
		Sound swordSound;
		Sound arrowSound;
		Sound fireBallSound;
		Sound iceBallSound;
		Sound thunderSound;

		const float musicDefaultVolume = 0.6f;
		const float sfxDefaultVolume = 0.8f;

		float musicVolume = musicDefaultVolume;
		float sfxVolume = sfxDefaultVolume;

		void init()
		{
			InitAudioDevice();

			gameplayMusic = LoadMusicStream("res/assets/music/gameplay-soundtrack.mp3");
			menuMusic = LoadMusicStream("res/assets/music/menu-soundtrack.mp3");

			selectSound = LoadSound("res/assets/sounds/option.ogg");
			hitPlayerSound = LoadSound("res/assets/sounds/hit.ogg");
			swordSound = LoadSound("res/assets/sounds/sword.ogg");
			arrowSound = LoadSound("res/assets/sounds/arrow.ogg");
			fireBallSound = LoadSound("res/assets/sounds/fireball.ogg");
			iceBallSound = LoadSound("res/assets/sounds/iceball.ogg");
			thunderSound = LoadSound("res/assets/sounds/thunder.ogg");

			setMusicVolume(musicDefaultVolume);
			setSfxVolume(sfxDefaultVolume);
		}

		void deInit()
		{
			UnloadMusicStream(gameplayMusic);
			UnloadMusicStream(menuMusic);

			UnloadSound(selectSound);
			UnloadSound(hitPlayerSound);
			UnloadSound(swordSound);
			UnloadSound(arrowSound);
			UnloadSound(fireBallSound);
			UnloadSound(iceBallSound);
			UnloadSound(thunderSound);

			CloseAudioDevice();
		}

		void setMusicVolume(float volume)
		{
			musicVolume = volume;
			SetMusicVolume(gameplayMusic, volume);
			SetMusicVolume(menuMusic, volume);
		}

		void setSfxVolume(float volume)
		{
			sfxVolume = volume;
			SetSoundVolume(selectSound, volume);
			SetSoundVolume(hitPlayerSound, volume);
			SetSoundVolume(swordSound, volume);
			SetSoundVolume(arrowSound, volume);
			SetSoundVolume(fireBallSound, volume);
			SetSoundVolume(iceBallSound, volume);
			SetSoundVolume(thunderSound, volume);
		}
	}
}