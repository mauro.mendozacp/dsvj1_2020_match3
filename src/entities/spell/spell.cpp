#include "spell.h"
#include "game/game.h"

namespace match3
{
	namespace spell
	{
		Rectangle fireBallRec;
		Rectangle iceBallRec;
		Rectangle thunderRec;
		Rectangle shieldRec;

		void init()
		{
			fireBallRec.width = static_cast<float>(game::screenWidth / 24);
			fireBallRec.height = static_cast<float>(game::screenHeight / 16);
			fireBallRec.x = gameplay::player->getRec().x + gameplay::player->getRec().width;
			fireBallRec.y = warzone::floorY - (fireBallRec.height * 150 / 100);

			iceBallRec.width = static_cast<float>(game::screenWidth / 24);
			iceBallRec.height = static_cast<float>(game::screenHeight / 16);
			iceBallRec.x = gameplay::player->getRec().x + gameplay::player->getRec().width;
			iceBallRec.y = warzone::floorY - (iceBallRec.height * 150 / 100);

			thunderRec.width = warzone::rec.width / 8;
			thunderRec.height = warzone::floorY;
			thunderRec.x = gameplay::player->getRec().x + gameplay::player->getRec().width;
			thunderRec.y = 0;

			shieldRec.width = gameplay::player->getRec().width * 150 / 100;
			shieldRec.height = gameplay::player->getRec().height;
			shieldRec.x = gameplay::player->getRec().x - shieldRec.width / 8;
			shieldRec.y = warzone::floorY - shieldRec.height;
		}

		void add(SPELL_TYPE spellType, int quantity)
		{
			bool insufMana = true;
			Spell* spell;
			Texture2D auxTexture;

			switch (spellType)
			{
			case SPELL_TYPE::FIRE_BALL:
				if (gameplay::player->getMana() >= FIRE_BALL_MANA_COST)
				{
					insufMana = false;
					auxTexture = texture::fireBallSpell;
					auxTexture.width = static_cast<int>(fireBallRec.width);
					auxTexture.height = static_cast<int>(fireBallRec.height);

					spell = new Spell(fireBallRec, auxTexture, WHITE, FIRE_BALL_MANA_COST, FIRE_BALL_DAMAGE * quantity, spellType);
				}
				break;
			case SPELL_TYPE::ICE_BALL:
				if (gameplay::player->getMana() >= ICE_BALL_MANA_COST)
				{
					insufMana = false;
					auxTexture = texture::iceBallSpell;
					auxTexture.width = static_cast<int>(iceBallRec.width);
					auxTexture.height = static_cast<int>(iceBallRec.height);

					spell = new Spell(iceBallRec, auxTexture, WHITE, ICE_BALL_MANA_COST, ICE_BALL_DAMAGE * quantity, spellType);
				}
				break;
			case SPELL_TYPE::THUNDER:
				if (gameplay::player->getMana() >= THUNDER_MANA_COST)
				{
					insufMana = false;
					auxTexture = texture::thunderSpell;
					auxTexture.width = static_cast<int>(thunderRec.width);
					auxTexture.height = static_cast<int>(thunderRec.height);

					spell = new Spell(thunderRec, auxTexture, WHITE, THUNDER_MANA_COST, THUNDER_DAMAGE * quantity, spellType);
				}
				break;
			case SPELL_TYPE::SHIELD:
				if (gameplay::player->getMana() >= SHIELD_MANA_COST)
				{
					insufMana = false;
					auxTexture = texture::shieldSpell;
					auxTexture.width = static_cast<int>(shieldRec.width);
					auxTexture.height = static_cast<int>(shieldRec.height);

					spell = new Spell(shieldRec, auxTexture, WHITE, SHIELD_MANA_COST, SHIELD_DURATION * quantity, spellType);
				}
				break;
			case SPELL_TYPE::RESTORATION:
				break;
			default:
				break;
			}

			if (!insufMana)
			{
				gameplay::player->spells.push_back(spell);
			}
			else
			{
				std::cout << "insuficiente mana\n";
			}
		}

		void update()
		{
			int it = 0;

			for (Spell* s : gameplay::player->spells)
			{
				if (s != NULL)
				{
					if (s->getIsActive())
					{
						s->update();

						switch (s->getType())
						{
						case SPELL_TYPE::FIRE_BALL:
						case SPELL_TYPE::ICE_BALL:

							for (enemy::Enemy* e : warzone::enemies)
							{
								if (e != NULL)
								{
									if (!e->getIsDead())
									{
										if (CheckCollisionRecs(s->getRec(), e->getRec()))
										{
											e->hurt(s->getValue());

											if (s->getType() == SPELL_TYPE::ICE_BALL)
											{
												e->freeze();
											}

											delete s;
											s = NULL;

											gameplay::player->spells.erase(gameplay::player->spells.begin() + it);
											break;
										}
									}
								}
							}

							if (s != NULL)
							{
								if (s->getRec().x > warzone::rec.x + warzone::rec.width)
								{
									delete s;
									s = NULL;

									gameplay::player->spells.erase(gameplay::player->spells.begin() + it);
								}
							}
							break;
						case SPELL_TYPE::THUNDER:
							for (enemy::Enemy* e : warzone::enemies)
							{
								if (e != NULL)
								{
									if (!e->getIsDead() && !e->getIsHurt())
									{
										if (CheckCollisionRecs(s->getRec(), e->getRec()))
										{
											e->hurt(s->getValue());
										}
									}
								}
							}

							if (s->getTimer() >= THUNDER_DURATION)
							{
								delete s;
								s = NULL;
								gameplay::player->spells.erase(gameplay::player->spells.begin() + it);
							}

							break;
						case SPELL_TYPE::SHIELD:							
							if (s->getTimer() >= s->getValue())
							{
								gameplay::player->setIsShield(false);

								delete s;
								s = NULL;
								gameplay::player->spells.erase(gameplay::player->spells.begin() + it);
							}
							else
							{
								gameplay::player->setIsShield(true);
							}

							break;
						case SPELL_TYPE::RESTORATION:
							break;
						default:
							break;
						}
					}
				}

				it++;
			}
		}

		void show()
		{
			for (Spell* s : gameplay::player->spells)
			{
				if (s->getIsActive())
				{
					s->show();
				}
			}
		}

		Spell::Spell()
		{
			this->rec = Rectangle();
			this->texture = Texture2D();
			this->color = Color();
			this->manaCost = 0;
			this->value = 0;
			this->timer = 0.0f;
			this->type = SPELL_TYPE();
			this->isActive = false;
		}

		Spell::Spell(Rectangle rec, Texture2D texture, Color color, int manaCost, int value, SPELL_TYPE type)
		{
			this->rec = rec;
			this->texture = texture;
			this->color = color;
			this->manaCost = manaCost;
			this->value = value;
			this->timer = 0.0f;
			this->type = type;
			this->isActive = false;
		}

		Spell::~Spell()
		{
		}

		void Spell::update()
		{
			switch (type)
			{
			case SPELL_TYPE::FIRE_BALL:
				rec.x += FIRE_BALL_SPEED * gameplay::frameTime;
				break;
			case SPELL_TYPE::ICE_BALL:
				rec.x += ICE_BALL_SPEED * gameplay::frameTime;
				break;
			case SPELL_TYPE::THUNDER:
			case SPELL_TYPE::SHIELD:
				timer += gameplay::frameTime;
			case SPELL_TYPE::RESTORATION:
				break;
			default:
				break;
			}
		}

		void Spell::show()
		{
			DrawTextureEx(texture, getPosition(), 0.0f, 1.0f, color);

#if DEBUG
			DrawRectangleLinesEx(rec, 1, GREEN);
#endif // DEBUG
		}

		Rectangle Spell::getRec()
		{
			return this->rec;
		}

		void Spell::setRec(Rectangle rec)
		{
			this->rec = rec;
		}

		Vector2 Spell::getPosition()
		{
			return { this->rec.x, this->rec.y };
		}

		void Spell::setPosition(Vector2 position)
		{
			this->rec.x = position.x;
			this->rec.y = position.y;
		}

		Texture2D Spell::getTexture()
		{
			return this->texture;
		}

		void Spell::setTexture(Texture2D texture)
		{
			this->texture = texture;
		}

		Color Spell::getColor()
		{
			return this->color;
		}

		void Spell::setColor(Color color)
		{
			this->color = color;
		}

		int Spell::getManaCost()
		{
			return this->manaCost;
		}

		void Spell::setManaCost(int manaCost)
		{
			this->manaCost = manaCost;
		}

		int Spell::getValue()
		{
			return this->value;
		}

		void Spell::setValue(int value)
		{
			this->value = value;
		}

		float Spell::getTimer()
		{
			return this->timer;
		}

		void Spell::setTimer(float timer)
		{
			this->timer = timer;
		}

		SPELL_TYPE Spell::getType()
		{
			return this->type;
		}

		void Spell::setType(SPELL_TYPE type)
		{
			this->type = type;
		}

		bool Spell::getIsActive()
		{
			return this->isActive;
		}

		void Spell::setIsActive(bool isActive)
		{
			this->isActive = isActive;
		}
	}
}