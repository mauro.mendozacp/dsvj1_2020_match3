#ifndef INFO_H
#define INFO_H

#include "raylib.h"

namespace match3
{
	namespace info
	{
		void init();
		void update();
		void draw();
	}
}

#endif // !INFO_H