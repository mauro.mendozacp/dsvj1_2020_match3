#ifndef ARCHER_H
#define ARCHER_H

#include "raylib.h"
#include <vector>
#include "entities/enemy/enemy.h"

namespace match3
{
	namespace archer
	{
		void init();
		void add();

		class Archer : public enemy::Enemy
		{
		public:
			Archer();
			Archer(Rectangle rec, int life, int damage, float moveSpeed, float attackSpeed, float destinyX, int points, Sound attackSound);
			~Archer();

			void setAnimation(character::ANIMATION_STATE animationState);
			
		private:

		};

		
	}
}

#endif // !ARCHER_H