#ifndef SELECT_DIFFICULTY_H
#define SELECT_DIFFICULTY_H

#include "raylib.h"
#include "entities/option/option.h"

namespace match3
{
	namespace select_difficulty
	{
		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif // !SELECT_DIFFICULTY_H