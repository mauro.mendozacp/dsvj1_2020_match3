#ifndef FONT_H
#define FONT_H

#include "raylib.h"

namespace match3
{
	namespace font
	{
		extern Font title;
		extern Font option;

		extern float titleFontSize;
		extern float optionFontSize;
		extern float valueFontSize;

		extern float titleSpacing;
		extern float optionSpacing;
		extern float valueSpacing;

		extern Color titleColor;
		extern Color optionColorSelected;
		extern Color optionColorNoSelected;

		void init();
		void deInit();
	}
}

#endif // !FONT_H