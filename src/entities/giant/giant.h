#ifndef GIANT_H
#define GIANT_H

#include "raylib.h"
#include "entities/enemy/enemy.h"

namespace match3
{
	namespace giant
	{
		void init();
		void add();

		class Giant : public enemy::Enemy
		{
		public:
			Giant();
			Giant(Rectangle rec, int life, int damage, float moveSpeed, float attackSpeed, float destinyX, int points, Sound attackSound);
			~Giant();

			void setAnimation(character::ANIMATION_STATE animationState);
		private:

		};
	}
}

#endif // !GIANT_H