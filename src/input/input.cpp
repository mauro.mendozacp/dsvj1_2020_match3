#include "input.h"
#include "audio/audio.h"

namespace match3
{
	namespace input
	{
		Vector2 mousePos;
		int click = MOUSE_LEFT_BUTTON;

		bool checkClick()
		{
			if (IsMouseButtonPressed(click))
			{
				return true;
			}

			return false;
		}

		bool checkMouseOnRec(Rectangle rec)
		{
			if (CheckCollisionPointRec(mousePos, rec))
			{
				return true;
			}

			return false;
		}

		bool checkClickOnRec(Rectangle rec)
		{
			if (CheckCollisionPointRec(mousePos, rec))
			{
				if (checkClick())
				{
					PlaySound(audio::selectSound);
					return true;
				}
			}

			return false;
		}

		bool checkMouseDownOnRec(Rectangle rec)
		{
			if (checkMouseOnRec(rec))
			{
				if (IsMouseButtonDown(click))
				{
					return true;
				}
			}

			return false;
		}

		bool checkMouseReleased()
		{
			if (IsMouseButtonReleased(click))
			{
				return true;
			}

			return false;
		}

		void mousePositionUpdate()
		{
			mousePos = GetMousePosition();
		}
	}
}