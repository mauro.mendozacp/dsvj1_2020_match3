#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include "raylib.h"
#include "grid/grid.h"
#include "warzone/warzone.h"
#include "info/info.h"
#include "entities/player/player.h"
#include "entities/option/option.h"

namespace match3
{
	namespace gameplay
	{
		enum class DIFFICULTY
		{
			EASY,
			MEDIUM,
			HARD
		};

		extern option::Option* pause;
		extern player::Player* player;

		extern float frameTime;
		extern float timer;

		extern DIFFICULTY difficulty;

		extern bool startGame;

		void restartGame();

		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif // !GAMEPLAY_H