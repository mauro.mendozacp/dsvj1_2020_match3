#include "game.h"

namespace match3
{
	namespace game
	{
		GAME_STATUS gameStatus;
		const char* titleGame = "MATCH 3";

		int screenWidth = 840;
		int screenHeight = 750;
		int highScore = 0;

		static void init()
		{
			InitWindow(screenWidth, screenHeight, titleGame);

			audio::init();
			font::init();
			texture::init();

#if DEBUG
			audio::setMusicVolume(0.0f);
#endif // DEBUG

			SetTargetFPS(frames);

			highScore = LoadStorageValue(STORAGE_DATA::STORAGE_POSITION_HISCORE);

			gameStatus = GAME_STATUS::MAIN_MENU;
			main_menu::init();
		}

		static void updateAudio()
		{
			switch (gameStatus)
			{
			case GAME_STATUS::MAIN_MENU:
				UpdateMusicStream(audio::menuMusic);
				break;
			case GAME_STATUS::SELECT_DIFFICULTY:
				UpdateMusicStream(audio::menuMusic);
				break;
			case GAME_STATUS::GAMEPLAY:
				UpdateMusicStream(audio::gameplayMusic);
				break;
			case GAME_STATUS::SETTINGS:
				UpdateMusicStream(audio::menuMusic);
				break;
			case GAME_STATUS::CREDITS:
				UpdateMusicStream(audio::menuMusic);
				break;
			default:
				break;
			}
		}

		static void update()
		{
			input::mousePositionUpdate();
			updateAudio();

			switch (gameStatus)
			{
			case GAME_STATUS::MAIN_MENU:
				main_menu::update();
				break;
			case GAME_STATUS::SELECT_DIFFICULTY:
				select_difficulty::update();
				break;
			case GAME_STATUS::GAMEPLAY:
				gameplay::update();
				break;
			case GAME_STATUS::PAUSE:
				pause::update();
				break;
			case GAME_STATUS::GAMEOVER:
				gameover::update();
				break;
			case GAME_STATUS::SETTINGS:
				settings::update();
				break;
			case GAME_STATUS::CREDITS:
				credits::update();
				break;
			default:
				break;
			}
		}

		static void drawBackground()
		{
			switch (gameStatus)
			{
			case GAME_STATUS::MAIN_MENU:
			case GAME_STATUS::SETTINGS:
			case GAME_STATUS::PAUSE:
				DrawTexture(texture::menuBackground, 0, 0, WHITE);
				break;
			case GAME_STATUS::SELECT_DIFFICULTY:
				DrawTexture(texture::menuBackground, 0, 0, WHITE);
				DrawTexture(texture::selectDifficultyBackground, 0, 0, WHITE);
				break;
			case GAME_STATUS::CREDITS:
				DrawTexture(texture::menuBackground, 0, 0, WHITE);
				DrawTexture(texture::creditsBackground, 0, 0, WHITE);
				break;
			case GAME_STATUS::GAMEOVER:
				DrawTexture(texture::gameoverBackground, 0, 0, WHITE);
				break;
			default:
				break;
			}
		}

		static void draw()
		{
			BeginDrawing();

			ClearBackground(BLACK);

			drawBackground();

			switch (gameStatus)
			{
			case GAME_STATUS::MAIN_MENU:
				main_menu::draw();
				break;
			case GAME_STATUS::SELECT_DIFFICULTY:
				select_difficulty::draw();
				break;
			case GAME_STATUS::GAMEPLAY:
				gameplay::draw();
				break;
			case GAME_STATUS::PAUSE:
				pause::draw();
				break;
			case GAME_STATUS::GAMEOVER:
				gameover::draw();
				break;
			case GAME_STATUS::SETTINGS:
				settings::draw();
				break;
			case GAME_STATUS::CREDITS:
				credits::draw();
				break;
			default:
				break;
			}

			EndDrawing();
		}

		static void deInit()
		{
			CloseWindow();
			audio::deInit();
			font::deInit();
			texture::deInit();
			changeStatus(GAME_STATUS::EXIT);
		}

		void runGame()
		{
			init();
			while (!WindowShouldClose() && gameStatus != GAME_STATUS::EXIT)
			{
				update();
				draw();
			}
			deInit();
		}

		void changeStatus(GAME_STATUS newGameStatus)
		{
			switch (gameStatus)
			{
			case GAME_STATUS::MAIN_MENU:
				main_menu::deInit();
				break;
			case GAME_STATUS::SELECT_DIFFICULTY:
				select_difficulty::deInit();
				break;
			case GAME_STATUS::GAMEPLAY:
				gameplay::deInit();
				break;
			case GAME_STATUS::PAUSE:
				pause::deInit();
				break;
			case GAME_STATUS::GAMEOVER:
				gameover::deInit();
				break;
			case GAME_STATUS::SETTINGS:
				settings::deInit();
				break;
			case GAME_STATUS::CREDITS:
				credits::deInit();
				break;
			default:
				break;
			}

			switch (newGameStatus)
			{
			case GAME_STATUS::MAIN_MENU:
				main_menu::init();
				break;
			case GAME_STATUS::SELECT_DIFFICULTY:
				select_difficulty::init();
				break;
			case GAME_STATUS::GAMEPLAY:
				gameplay::init();
				break;
			case GAME_STATUS::PAUSE:
				pause::init();
				break;
			case GAME_STATUS::GAMEOVER:
				gameover::init();
				break;
			case GAME_STATUS::SETTINGS:
				settings::init();
				break;
			case GAME_STATUS::CREDITS:
				credits::init();
				break;
			default:
				break;
			}

			gameStatus = newGameStatus;
		}
	}
}