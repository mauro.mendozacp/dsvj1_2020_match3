#ifndef SETTINGS_H
#define SETTINGS_H

#include "raylib.h"

namespace match3
{
	namespace settings
	{
		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif // !SETTINGS_H