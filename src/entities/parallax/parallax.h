#ifndef PARALLAX_H
#define PARALLAX_H

#include "raylib.h"

namespace match3
{
	namespace parallax
	{
		const int parallaxLenght = 3;

		class Parallax
		{
		public:
			Parallax();
			Parallax(Rectangle rec, Texture2D texture, float speed);
			~Parallax();

			void move();
			void show();

			Rectangle getRec();
			void setRec(Rectangle rec);
			Vector2 getPosition();
			void setPosition(Vector2 position);
			Texture2D getTexture();
			void setTexture(Texture2D texture);
			float getSpeed();
			void setSpeed(float cloudspeedSpeed);

		private:
			Rectangle rec;
			Texture2D texture;
			float positionX[parallaxLenght];
			float speed;
		};
	}
}

#endif // !PARALLAX_H