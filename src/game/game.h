#ifndef GAME_H
#define GAME_H

#include <iostream>

#include "raylib.h"
#include "audio/audio.h"
#include "texture/texture.h"
#include "font/font.h"
#include "input/input.h"

#include "scenes/main_menu/main_menu.h"
#include "scenes/gameplay/gameplay.h"
#include "scenes/pause/pause.h"
#include "scenes/gameover/gameover.h"
#include "scenes/settings/settings.h"
#include "scenes/credits/credits.h"
#include "scenes/select_difficulty/select_difficulty.h"

namespace match3
{
	namespace game
	{
		enum STORAGE_DATA
		{
			STORAGE_POSITION_HISCORE = 0
		};

		enum class GAME_STATUS
		{
			GAMEPLAY = 1,
			MAIN_MENU,
			PAUSE,
			SELECT_DIFFICULTY,
			GAMEOVER,
			SETTINGS,
			CREDITS,
			EXIT
		};

		extern GAME_STATUS gameStatus;
		extern const char* titleGame;

		extern int screenWidth;
		extern int screenHeight;
		const int frames = 60;
		extern int highScore;

		void runGame();
		void changeStatus(GAME_STATUS newGameStatus);
	}
}

#endif // !GAME_H