#include "bar.h"

namespace match3
{
	namespace bar
	{
		Bar::Bar()
		{
			this->rec = Rectangle();
			this->maxRec = Rectangle();
			this->color = Color();
		}

		Bar::Bar(Rectangle rec, Rectangle maxRec, Color color)
		{
			this->rec = rec;
			this->maxRec = maxRec;
			this->color = color;
		}

		Bar::~Bar()
		{
		}

		bool Bar::collision(Rectangle rec)
		{
			return CheckCollisionRecs(this->rec, rec);
		}

		void Bar::update(int value, int maxValue)
		{
			float porc = static_cast<float>(value * 100 / maxValue);
			rec.width = porc * maxRec.width / 100;
		}

		void Bar::show()
		{
			DrawRectangleRec(rec, color);

			DrawRectangleLinesEx(maxRec, 1, BLACK);
		}

		Rectangle Bar::getRec()
		{
			return this->rec;
		}

		void Bar::setRec(Rectangle rec)
		{
			this->rec = rec;
		}

		Rectangle Bar::getMaxRec()
		{
			return this->maxRec;
		}

		void Bar::setMaxRec(Rectangle maxRec)
		{
			this->maxRec = maxRec;
		}

		void Bar::setPositionX(float positionX)
		{
			this->rec.x = positionX;
			this->maxRec.x = positionX;
		}

		Color Bar::getColor()
		{
			return this->color;
		}

		void Bar::setColor(Color color)
		{
			this->color = color;
		}
	}
}