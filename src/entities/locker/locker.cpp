#include "locker.h"
#include <vector>
#include "input/input.h"
#include "texture/texture.h"
#include "scenes/gameplay/gameplay.h"

namespace match3
{
	namespace locker
	{
		float lockerWidth;
		float lockerHeight;

		Color lockerColorSelected = GREEN;
		Color lockerColor = WHITE;

		const float lockerSpeed = 100.0f;
		const int lockerRestProb = 2;

		void reset()
		{
			for (int i = 0; i < grid::gridRow; i++)
			{
				for (int j = 0; j < grid::gridCol; j++)
				{
					delete grid::locker[i][j];
					grid::locker[i][j] = NULL;
				}
			}

			locker::init();
		}

		void add(int i, int j, int counterI)
		{
			Rectangle auxRec = Rectangle();
			auxRec.width = lockerWidth;
			auxRec.height = lockerHeight;
			auxRec.x = grid::rec.x + (j * lockerWidth);
			auxRec.y = grid::rec.y - ((counterI + 1) * lockerHeight);

			float auxDestPosY = grid::rec.y + (i * lockerHeight);

			Texture2D auxLockerBG = texture::lockerBackground;
			auxLockerBG.width = static_cast<int>(auxRec.width);
			auxLockerBG.height = static_cast<int>(auxRec.height);

			int lockerTypeRand = 1;
			if (GetRandomValue(1, 100) <= lockerRestProb)
			{
				lockerTypeRand = lockerTypesLenght;
			}
			else
			{
				lockerTypeRand = GetRandomValue(1, lockerTypesLenght - 1);
			}

			grid::locker[i][j] = new Locker(auxRec, auxDestPosY, auxLockerBG, lockerColor, static_cast<LOCKER_TYPE>(lockerTypeRand), lockerSpeed);
		}

		void init()
		{
			lockerWidth = grid::rec.width / grid::gridCol;
			lockerHeight = grid::rec.height / grid::gridRow;
			
			Rectangle auxRec = Rectangle();
			auxRec.width = lockerWidth;
			auxRec.height = lockerHeight;

			Texture2D auxLockerBG = texture::lockerBackground;
			auxLockerBG.width = static_cast<int>(auxRec.width);
			auxLockerBG.height = static_cast<int>(auxRec.height);

			int lockerTypeRand = 1;

			for (int i = 0; i < grid::gridRow; i++)
			{
				for (int j = 0; j < grid::gridCol; j++)
				{
					auxRec.x = grid::rec.x + (j * lockerWidth);
					auxRec.y = grid::rec.y + (i * lockerHeight);

					if (GetRandomValue(1, 100) <= lockerRestProb)
					{
						lockerTypeRand = lockerTypesLenght;
					}
					else
					{
						lockerTypeRand = GetRandomValue(1, lockerTypesLenght - 1);
					}

					grid::locker[i][j] = new Locker(auxRec, auxRec.y, auxLockerBG, lockerColor, static_cast<LOCKER_TYPE>(lockerTypeRand), lockerSpeed);
				}
			}
		}

		void update()
		{
			for (int i = 0; i < grid::gridRow; i++)
			{
				for (int j = 0; j < grid::gridCol; j++)
				{
					grid::locker[i][j]->move();
				}
			}
		}

		void draw()
		{
			for (int i = 0; i < grid::gridRow; i++)
			{
				for (int j = 0; j < grid::gridCol; j++)
				{
					grid::locker[i][j]->show();
				}
			}
		}

		void deInit()
		{
			for (int i = 0; i < grid::gridRow; i++)
			{
				for (int j = 0; j < grid::gridCol; j++)
				{
					if (grid::locker[i][j] != NULL)
					{
						delete grid::locker[i][j];
					}
				}
			}
		}

		Locker::Locker()
		{
			this->rec = Rectangle();
			this->destPositionY = 0.0f;
			this->texture = Texture2D();
			this->background = Texture2D();
			this->color = Color();
			this->lockerType = LOCKER_TYPE();
			this->selected = false;
			this->speed = 0.0f;
		}

		Locker::Locker(Rectangle rec, float destPositionY, Texture2D background, Color color, LOCKER_TYPE lockerType, float speed)
		{
			this->rec = rec;
			this->destPositionY = destPositionY;
			this->background = background;
			this->color = color;
			this->lockerType = lockerType;
			this->selected = false;
			this->speed = speed;

			this->texture = getTextureForLockerType();
		}

		Locker::~Locker()
		{
		}

		void Locker::move()
		{
			if (rec.y < destPositionY)
			{
				rec.y += speed * gameplay::frameTime;

				if (rec.y > destPositionY)
				{
					rec.y = destPositionY;
				}
			}
		}

		void Locker::show()
		{
			DrawTextureEx(background, getPos(), 0.0f, 1.0f, color);
			DrawTextureEx(texture, getPos(), 0.0f, 1.0f, color);

#if DEBUG
			DrawRectangleLinesEx(rec, 1, GREEN);
#endif // DEBUG
		}

		Rectangle Locker::getRec()
		{
			return this->rec;
		}

		void Locker::setRec(Rectangle rec)
		{
			this->rec = rec;
		}

		Vector2 Locker::getPos()
		{
			return { rec.x, rec.y };
		}

		void Locker::setPos(Vector2 pos)
		{
			rec.x = pos.x;
			rec.y = pos.y;
		}

		float Locker::getDestPosY()
		{
			return this->destPositionY;
		}

		void Locker::setDestPosY(float destPositionY)
		{
			this->destPositionY = destPositionY;
		}

		Texture2D Locker::getBackground()
		{
			return this->background;
		}

		void Locker::setBackground(Texture2D background)
		{
			this->background = background;
		}

		Texture2D Locker::getTexture()
		{
			return this->texture;
		}

		void Locker::setTexture(Texture2D texture)
		{
			this->texture = texture;
		}

		Color Locker::getColor()
		{
			return this->color;
		}

		void Locker::setColor(Color color)
		{
			this->color = color;
		}

		LOCKER_TYPE Locker::getLockerType()
		{
			return this->lockerType;
		}

		void Locker::setLockerType(LOCKER_TYPE lockerType)
		{
			this->lockerType = lockerType;
		}

		bool Locker::getSelected()
		{
			return this->selected;
		}

		void Locker::setSelected(bool selected)
		{
			this->selected = selected;
		}

		float Locker::getSpeed()
		{
			return this->speed;
		}

		void Locker::setSpeed(float speed)
		{
			this->speed = speed;
		}

		Texture2D Locker::getTextureForLockerType()
		{
			Texture2D auxTexture;

			switch (this->lockerType)
			{
			case LOCKER_TYPE::LIFE_POTION:
				auxTexture = texture::lockerLifePotion;
				break;
			case LOCKER_TYPE::MANA_POTION:
				auxTexture = texture::lockerManaPotion;
				break;
			case LOCKER_TYPE::FIRE_BALL:
				auxTexture = texture::lockerFireBall;
				break;
			case LOCKER_TYPE::ICE_BALL:
				auxTexture = texture::lockerIceBall;
				break;
			case LOCKER_TYPE::THUNDER:
				auxTexture = texture::lockerThunder;
				break;
			case LOCKER_TYPE::SHIELD:
				auxTexture = texture::lockerShield;
				break;
			case LOCKER_TYPE::RESTORATION:
				auxTexture = texture::lockerRestoration;
				break;
			default: auxTexture = Texture2D();
				break;
			}

			auxTexture.width = static_cast<int>(rec.width);
			auxTexture.height = static_cast<int>(rec.height);

			return auxTexture;
		}

		static void checkLocker(LockerIndex lIndex, LockerIndex lcIndex, std::vector<LockerIndex>& auxLockerList);
		static void checkLockerAround(LockerIndex lIndex, LockerIndex lbIndex, std::vector<LockerIndex>& auxLockerList)
		{
			for (int k = lIndex.i - 1; k <= lIndex.i + 1; k++)
			{
				for (int h = lIndex.j - 1; h <= lIndex.j + 1; h++)
				{
					if ((k == lIndex.i && h == lIndex.j) || (k == lbIndex.i && h == lbIndex.j))
					{
						continue;
					}
					checkLocker({ lIndex.i, lIndex.j }, { k, h }, auxLockerList);
				}
			}
		}

		static void checkLocker(LockerIndex lIndex, LockerIndex lcIndex, std::vector<LockerIndex>& auxLockerList)
		{
			if (lcIndex.i >= 0 && lcIndex.i < grid::gridRow && lcIndex.j >= 0 && lcIndex.j < grid::gridCol)
			{
				if (grid::locker[lIndex.i][lIndex.j]->getLockerType() == grid::locker[lcIndex.i][lcIndex.j]->getLockerType())
				{
					for (LockerIndex l : auxLockerList)
					{
						if (l.i == lcIndex.i && l.j == lcIndex.j)
						{
							return;
						}
					}

					auxLockerList.push_back(lcIndex);
					checkLockerAround(lcIndex, lIndex, auxLockerList);
				}
			}
		}

		bool checkLockerAvaibles()
		{
			std::vector<LockerIndex> auxLockerList;

			for (int i = 0; i < grid::gridRow; i++)
			{
				for (int j = 0; j < grid::gridCol; j++)
				{
					auxLockerList.clear();

					checkLockerAround({ i, j }, { -1, -1 }, auxLockerList);

					if (auxLockerList.size() >= minSelectLocker)
					{
						std::cout << "i: " << i << ", j: " << j << " ->" << auxLockerList.size() << std::endl;
						return true;
					}
				}
			}

			return false;
		}
	}
}