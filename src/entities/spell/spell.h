#ifndef SPELL_H
#define SPELL_H

#include "raylib.h"

namespace match3
{
	namespace spell
	{
		extern Rectangle fireBallRec;
		extern Rectangle iceBallRec;

		enum class SPELL_TYPE
		{
			FIRE_BALL = 1,
			ICE_BALL,
			THUNDER,
			SHIELD,
			RESTORATION
		};

		void init();
		void update();
		void show();
		void add(SPELL_TYPE spellType, int quantity);

		class Spell
		{
		public:
			Spell();
			Spell(Rectangle rec, Texture2D texture, Color color, int manaCost, int value, SPELL_TYPE type);
			~Spell();

			void update();
			void show();

			Rectangle getRec();
			void setRec(Rectangle rec);
			Vector2 getPosition();
			void setPosition(Vector2 position);
			Texture2D getTexture();
			void setTexture(Texture2D texture);
			Color getColor();
			void setColor(Color color);
			int getManaCost();
			void setManaCost(int manaCost);
			int getValue();
			void setValue(int value);
			float getTimer();
			void setTimer(float timer);
			SPELL_TYPE getType();
			void setType(SPELL_TYPE type);
			bool getIsActive();
			void setIsActive(bool isActive);

		private:
			Rectangle rec;
			Texture2D texture;
			Color color;
			int manaCost;
			int value;
			float timer;
			SPELL_TYPE type;
			bool isActive;
		};
	}
}

#endif // !SPELL_H