#include "character.h"

namespace match3
{
	namespace character
	{
		Color normalColor = WHITE;
		Color hurtColor = RED;
		Color freezeColor = SKYBLUE;

		Character::Character()
		{
			this->rec = Rectangle();
			this->animationState = ANIMATION_STATE();
			this->animation = animation::Animation();
			this->lifeBar = bar::Bar();
			this->life = 0;
			this->maxLife = 0;
			this->color = Color();
			this->isAttack = false;
			this->isHurt = false;
			this->isDead = false;
		}

		Character::Character(Rectangle rec, int life, Color color)
		{
			this->rec = rec;
			this->animationState = ANIMATION_STATE::IDLE;
			this->life = life;
			this->maxLife = life;
			this->color = color;
			this->isAttack = false;
			this->isHurt = false;
			this->isDead = false;

			setLifeBarForRec(rec);
		}

		Character::~Character()
		{
		}

		void Character::updateAnimation()
		{
			animation.update();
		}

		void Character::show()
		{
			animation.draw(getPosition(), color);

#if DEBUG
			DrawRectangleLinesEx(rec, 1, GREEN);
#endif // DEBUG

			lifeBar.show();
		}

		void Character::dead()
		{
			isDead = true;
			setAnimation(character::ANIMATION_STATE::DEAD);
		}

		void Character::hurt(int damage)
		{
			color = hurtColor;
			isHurt = true;
			setAnimation(character::ANIMATION_STATE::HURT);
			setLife(life - damage);
		}

		Rectangle Character::getRec()
		{
			return this->rec;
		}

		void Character::setRec(Rectangle rec)
		{
			this->rec = rec;
		}

		Vector2 Character::getPosition()
		{
			return { this->rec.x, this->rec.y };
		}

		void Character::setPosition(Vector2 position)
		{
			this->rec.x = position.x;
			this->rec.y = position.y;
		}

		ANIMATION_STATE Character::getAnimationState()
		{
			return this->animationState;
		}

		void Character::setAnimationState(ANIMATION_STATE animationState)
		{
			this->animationState = animationState;
		}

		animation::Animation Character::getAnimation()
		{
			return this->animation;
		}

		bar::Bar Character::getLifeBar()
		{
			return this->lifeBar;
		}

		void Character::setLifeBarForRec(Rectangle rec)
		{
			rec.height = bar::barHeight;
			rec.y -= rec.height * 3;
			this->lifeBar = bar::Bar(rec, rec, bar::lifeBarColor);
		}

		int Character::getLife()
		{
			return this->life;
		}

		void Character::setLife(int life)
		{
			if (!isDead)
			{
				if (life <= 0)
				{
					this->life = 0;
					dead();
				}
				else if (life >= maxLife)
				{
					this->life = maxLife;
				}
				else
				{
					this->life = life;
				}

				lifeBar.update(this->life, maxLife);
			}
		}

		int Character::getMaxLife()
		{
			return this->maxLife;
		}

		void Character::setMaxLife(int maxLife)
		{
			this->maxLife = maxLife;
		}

		Color Character::getColor()
		{
			return this->color;
		}

		void Character::setColor(Color color)
		{
			this->color = color;
		}

		bool Character::getIsAttack()
		{
			return this->isAttack;
		}

		void Character::setIsAttack(bool isAttack)
		{
			this->isAttack = isAttack;
		}

		bool Character::getIsHurt()
		{
			return this->isHurt;
		}

		void Character::setIsHurt(bool isHurt)
		{
			this->isHurt = isHurt;
		}

		bool Character::getIsDead()
		{
			return this->isDead;
		}
		void Character::setIsDead(bool isDead)
		{
			this->isDead = isDead;
		}
	}
}