#ifndef CREDITS_H
#define CREDITS_H

#include "raylib.h"

namespace match3
{
	namespace credits
	{
		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif // !CREDITS_H