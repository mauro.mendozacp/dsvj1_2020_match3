#ifndef GRID_H
#define GRID_H

#include "raylib.h"
#include "entities/locker/locker.h"

namespace match3
{
	namespace grid
	{
		const int gridRow = 8;
		const int gridCol = 7;

		extern locker::Locker* locker[gridRow][gridCol];
		extern Rectangle rec;
		extern Texture2D background;

		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif // !GRID_H