#include "warzone.h"
#include "game/game.h"

namespace match3
{
	namespace warzone
	{
		Rectangle rec;
		Color color;
		Texture2D background;
		Texture2D floorTexture;
		parallax::Parallax* clouds;
		float floorY;
		float playerStartPositionX;
		float enemyStartPositionX;
		animation::Animation countdown;
		Vector2 countdownPos;
		
		std::vector<enemy::Enemy*> enemies;
		float respawnEnemiesTimer;
		float respawnEnemies;

		void init()
		{
			respawnEnemiesTimer = 0.0f;

			rec.x = 0.0f;
			rec.y = 0.0f;
			rec.width = static_cast<float>(game::screenWidth);
			rec.height = static_cast<float>(game::screenHeight / 2);

			color = WHITE;

			floorY = rec.height * 3 / 4;

			background = texture::warZoneBackground;
			background.width = static_cast<int>(rec.width);
			background.height = static_cast<int>(floorY);

			floorTexture = texture::floor;
			floorTexture.width = static_cast<int>(rec.width);
			floorTexture.height = static_cast<int>(rec.height / 4);

			playerStartPositionX = rec.width / 8;

			enemyStartPositionX = rec.width;

			//Clouds
			Texture2D auxClouds = texture::clouds;
			auxClouds.width = static_cast<int>(rec.width);
			auxClouds.height = static_cast<int>(rec.height);

			float auxCloudsSpeed = 50.0f;

			clouds = new parallax::Parallax(rec, auxClouds, auxCloudsSpeed);

			Rectangle auxPlayerRec = { 0.0f, 0.0f, rec.width / 6, rec.height / 4 };
			int auxFramesX = 8;
			int auxFramesY = 6;
			Texture2D auxTexture = texture::countdown;
			auxTexture.width = static_cast<int>(auxPlayerRec.width * auxFramesX);
			auxTexture.height = static_cast<int>(auxPlayerRec.height * auxFramesY);

			countdown = animation::Animation(auxTexture, auxPlayerRec, (auxFramesX * auxFramesY), 1000.0f, false);

			countdownPos.x = rec.width / 2 - (auxPlayerRec.width / 2);
			countdownPos.y = rec.height / 2 - (auxPlayerRec.height / 2);

			switch (gameplay::difficulty)
			{
			case gameplay::DIFFICULTY::EASY:
				respawnEnemies = RESPAWN_ENEMIES_EASY;
				break;
			case gameplay::DIFFICULTY::MEDIUM:
				respawnEnemies = RESPAWN_ENEMIES_MEDIUM;
				break;
			case gameplay::DIFFICULTY::HARD:
				respawnEnemies = RESPAWN_ENEMIES_HARD;
				break;
			default:
				break;
			}

			enemy::init();
		}

		void update()
		{
			clouds->move();

			gameplay::player->update();
			enemy::update();
			spell::update();

			respawnEnemiesTimer += gameplay::frameTime;
			if (respawnEnemiesTimer >= respawnEnemies)
			{
				respawnEnemiesTimer = 0.0f;

				enemy::add();
			}
		}

		void draw()
		{
			DrawTextureEx(background, { rec.x, rec.y }, 0.0f, 1.0f, color);

			DrawTextureEx(floorTexture, { rec.x, floorY }, 0.0f, 1.0f, WHITE);

			clouds->show();
			gameplay::player->show();
			enemy::draw();
			spell::show();

			if (!gameplay::startGame)
			{
				countdown.draw(countdownPos, WHITE);
			}
		}

		void deInit()
		{
			if (clouds != NULL)
			{
				delete clouds;
			}
			enemy::deInit();
		}
	}
}