#include "gameover.h"
#include "game/game.h"
#include "entities/option/option.h"

namespace match3
{
	namespace gameover
	{
		enum class GAMEOVER_MENU
		{
			RESTART = 1,
			GO_BACK
		};

		const int optionLenght = 2;
		option::Option* title;
		option::Option* options[optionLenght];
		option::Option* highscore;

		std::string titleText = "GAME OVER";
		std::string optionTexts[optionLenght] = { "RESTART", "BACK TO MENU" };

		static void acceptOption(GAMEOVER_MENU option)
		{
			switch (option)
			{
			case GAMEOVER_MENU::RESTART:
				gameplay::restartGame();
				game::gameStatus = game::GAME_STATUS::GAMEPLAY;
				deInit();
				break;
			case GAMEOVER_MENU::GO_BACK:
				gameplay::deInit();
				game::changeStatus(game::GAME_STATUS::MAIN_MENU);
				break;
			default:
				break;
			}
		}

		void init()
		{
			StopMusicStream(audio::gameplayMusic);
			option::init(title, titleText, options, optionLenght, optionTexts);

			//Highscore
			std::string text;
			if (gameplay::player->getScore() > game::highScore)
			{
				text = TextFormat("NEW HIGHSCORE: %i", gameplay::player->getScore());
			}
			else
			{
				text = TextFormat("YOU SCORE: %i", gameplay::player->getScore());
			}
			option::highscoreInit(highscore, text);


			if (gameplay::player->getScore() > game::highScore)
			{
				SaveStorageValue(game::STORAGE_DATA::STORAGE_POSITION_HISCORE, gameplay::player->getScore());
			}
		}

		void update()
		{
			option::update(options, optionLenght);
			acceptOption(static_cast<GAMEOVER_MENU>(option::getOption(options, optionLenght)));
		}

		void draw()
		{
			option::draw(title, options, optionLenght);
			highscore->show();
		}

		void deInit()
		{
			option::deInit(title, options, optionLenght);
			option::highscoreDeInit(highscore);
			game::highScore = LoadStorageValue(game::STORAGE_DATA::STORAGE_POSITION_HISCORE);
		}
	}
}