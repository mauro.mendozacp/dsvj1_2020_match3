#ifndef CONTANTS_H
#define CONTANTS_H

namespace match3
{
//Player
#define PLAYER_LIFE 1000
#define PLAYER_MANA 250
#define PLAYER_SHIELD 150
#define PLAYER_REG_MANA 25
#define PLAYER_DAMAGE 50

//Spells
#define FIRE_BALL_DAMAGE 75
#define FIRE_BALL_SPEED 200.0f
#define FIRE_BALL_MANA_COST 60

#define ICE_BALL_DAMAGE 50
#define ICE_BALL_SPEED 200.0f
#define ICE_BALL_FREEZING 3.0f
#define ICE_BALL_MANA_COST 50

#define SHIELD_DURATION 3
#define SHIELD_MANA_COST 40

#define THUNDER_DAMAGE 100
#define THUNDER_DURATION 0.5f
#define THUNDER_MANA_COST 80

//Potions
#define LIFE_POTION_VALUE 100
#define MANA_POTION_VALUE 50

//WarZone
#define RESPAWN_ENEMIES_EASY 10.0f
#define RESPAWN_ENEMIES_MEDIUM 8.0f
#define RESPAWN_ENEMIES_HARD 5.0f

#define ENEMY_WARRIOR_RESP_EASY 90
#define ENEMY_GIANT_RESP_EASY 10
#define ENEMY_ARCHER_RESP_EASY 0

#define ENEMY_WARRIOR_RESP_MEDIUM 80
#define ENEMY_GIANT_RESP_MEDIUM 15
#define ENEMY_ARCHER_RESP_MEDIUM 5

#define ENEMY_WARRIOR_RESP_HARD 65
#define ENEMY_GIANT_RESP_HARD 25
#define ENEMY_ARCHER_RESP_HARD 10

//Warrior Enemy
#define ENEMY_WARRIOR_LIFE 200
#define ENEMY_WARRIOR_DAMAGE 75
#define ENEMY_WARRIOR_MOVE_SPEED 100.0f
#define ENEMY_WARRIOR_ATTACK_SPEED 2.0f
#define ENEMY_WARRIOR_POINTS 100

//Giant Enemy
#define ENEMY_GIANT_LIFE 400
#define ENEMY_GIANT_DAMAGE 100
#define ENEMY_GIANT_MOVE_SPEED 75.0f
#define ENEMY_GIANT_ATTACK_SPEED 5.0f
#define ENEMY_GIANT_POINTS 250

//Archer Enemy
#define ENEMY_ARCHER_LIFE 150
#define ENEMY_ARCHER_DAMAGE 50
#define ENEMY_ARCHER_MOVE_SPEED 80.0f
#define ENEMY_ARCHER_ATTACK_SPEED 8.0f
#define ENEMY_ARCHER_POINTS 150
}

#endif // !CONTANTS_H