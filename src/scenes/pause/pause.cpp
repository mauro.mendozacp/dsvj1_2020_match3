#include "pause.h"
#include "game/game.h"
#include "entities/option/option.h"

namespace match3
{
	namespace pause
	{
		enum class PAUSE_MENU
		{
			RESUME = 1,
			GO_BACK
		};

		const int optionLenght = 2;
		option::Option* title;
		option::Option* options[optionLenght];

		std::string titleText = "PAUSE";
		std::string optionTexts[optionLenght] = { "RESUME", "BACK TO MENU" };

		static void acceptOption(PAUSE_MENU option)
		{
			switch (option)
			{
			case PAUSE_MENU::RESUME:
				ResumeMusicStream(audio::gameplayMusic);
				game::gameStatus = game::GAME_STATUS::GAMEPLAY;
				deInit();
				break;
			case PAUSE_MENU::GO_BACK:
				StopMusicStream(audio::gameplayMusic);
				gameplay::deInit();
				game::changeStatus(game::GAME_STATUS::MAIN_MENU);
				break;
			default:
				break;
			}
		}

		void init()
		{
			PauseMusicStream(audio::gameplayMusic);
			option::init(title, titleText, options, optionLenght, optionTexts);
		}

		void update()
		{
			option::update(options, optionLenght);
			acceptOption(static_cast<PAUSE_MENU>(option::getOption(options, optionLenght)));
		}

		void draw()
		{
			option::draw(title, options, optionLenght);
		}

		void deInit()
		{
			option::deInit(title, options, optionLenght);
		}
	}
}