#include "player.h"
#include "audio/audio.h"
#include "texture/texture.h"
#include "input/input.h"
#include "scenes/gameplay/gameplay.h"

namespace match3
{
	namespace player
	{
		float recWidth;
		float recHeight;

		Color lockerColorNoSelected = WHITE;
		Color lockerColorSelected = GRAY;

		float timer;
		const float regManCoolDown = 3.0f;

		void init()
		{
			timer = 0.0f;

			recWidth = warzone::rec.width / 16;
			recHeight = warzone::rec.height / 4;

			Rectangle auxRec;
			auxRec.width = recWidth;
			auxRec.height = recHeight;
			auxRec.x = warzone::playerStartPositionX;
			auxRec.y = warzone::floorY - auxRec.height;

			gameplay::player = new Player(auxRec, PLAYER_LIFE, PLAYER_MANA);

			spell::init();
		}

		void deInit()
		{
			for (spell::Spell* s : gameplay::player->spells)
			{
				if (s != NULL)
				{
					delete s;
					s = NULL;
				}
			}
			gameplay::player->spells.clear();
			if (gameplay::player != NULL)
			{
				delete gameplay::player;
			}
		}

		Player::Player(): Character()
		{
			this->mana = 0;
			this->score = 0;
			this->selected = false;
			this->isShield = false;
		}

		Player::Player(Rectangle rec, int life, int mana)
			: Character(rec, life, WHITE)
		{
			setAnimation(character::ANIMATION_STATE::IDLE);
			this->mana = mana;
			this->score = 0;
			this->selected = false;
			this->isShield = false;

			setManaBarForRec(rec);
		}

		Player::~Player()
		{
		}

		void Player::attack()
		{
			if (!isAttack)
			{
				for (spell::Spell* s : spells)
				{
					if (!s->getIsActive())
					{
						setMana(mana - s->getManaCost());
						s->setIsActive(true);
						isAttack = true;

						switch (s->getType())
						{
						case spell::SPELL_TYPE::FIRE_BALL:
							PlaySound(audio::fireBallSound);
							break;
						case spell::SPELL_TYPE::ICE_BALL:
							PlaySound(audio::iceBallSound);
							break;
						case spell::SPELL_TYPE::THUNDER:
							PlaySound(audio::thunderSound);
							break;
						case spell::SPELL_TYPE::SHIELD:
							isShield = true;
							break;
						default:
							break;
						}

						if (s->getType() == spell::SPELL_TYPE::SHIELD)
						{
							isShield = true;
						}

						setAnimation(character::ANIMATION_STATE::ATTACK);
						break;
					}
				}
			}
		}

		void Player::update()
		{
			if (!isDead)
			{
				attack();

				if (isHurt)
				{
					if (animation.getIsFinish())
					{
						isHurt = false;
						isAttack = false;
						color = WHITE;
						setAnimation(character::ANIMATION_STATE::IDLE);
					}
				}
				if (isAttack)
				{
					if (animation.getIsFinish())
					{
						isAttack = false;
						setAnimation(character::ANIMATION_STATE::IDLE);
					}
				}

				timer += gameplay::frameTime;
				if (timer >= regManCoolDown)
				{
					setMana(mana + PLAYER_REG_MANA);
					timer = 0.0f;
				}
			}
			
			updateAnimation();
		}

		void Player::show()
		{
			character::Character::show();
			manaBar.show();
		}

		void Player::clickOnFirstLocker()
		{
			if (input::checkClickOnRec(grid::rec))
			{
				selected = true;

				for (int i = 0; i < grid::gridRow; i++)
				{
					for (int j = 0; j < grid::gridCol; j++)
					{
						if (input::checkMouseOnRec(grid::locker[i][j]->getRec()))
						{
							grid::locker[i][j]->setColor(lockerColorSelected);
							lockerSelected.push_back({ i, j });
							std::cout << i << ", " << j << std::endl;
							return;
						}
					}
				}
			}
		}

		void Player::selectLocker()
		{
			if (selected)
			{
				for (int i = 0; i < grid::gridRow; i++)
				{
					for (int j = 0; j < grid::gridCol; j++)
					{
						if (input::checkMouseOnRec(grid::locker[i][j]->getRec()))
						{
							if (lockerSelected.size() > 0)
							{
								if (grid::locker[i][j]->getLockerType() == grid::locker[lockerSelected.front().i][lockerSelected.front().j]->getLockerType())
								{
									bool lockerExist = false;
									for (LockerIndex lock : lockerSelected)
									{
										if (lock.i == i && lock.j == j)
										{
											lockerExist = true;
											continue;
										}

										if (lockerExist)
										{
											grid::locker[lock.i][lock.j]->setColor(lockerColorNoSelected);
											lockerSelected.pop_back();
											std::cout << "erased: " << lock.i << ", " << lock.j << std::endl;
										}
									}

									if (!lockerExist)
									{
										if (i >= lockerSelected.back().i - 1 && i <= lockerSelected.back().i + 1)
										{
											if (j >= lockerSelected.back().j - 1 && j <= lockerSelected.back().j + 1)
											{
												grid::locker[i][j]->setColor(lockerColorSelected);
												lockerSelected.push_back({ i, j });
												std::cout << "push: " << i << ", " << j << std::endl;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		void Player::matchLocker()
		{
			if (input::checkMouseReleased() && selected)
			{
				if (!isDead)
				{
					bool match = false;
					selected = false;

					if (lockerSelected.size() >= minSelectLocker)
					{
						match = true;

						makeSkill(grid::locker[lockerSelected.front().i][lockerSelected.front().j]->getLockerType(), lockerSelected.size());

						for (LockerIndex lock : lockerSelected)
						{
							grid::locker[lock.i][lock.j] = NULL;
						}
					}

					for (LockerIndex lock : lockerSelected)
					{
						if (grid::locker[lock.i][lock.j] != NULL)
						{
							grid::locker[lock.i][lock.j]->setColor(lockerColorNoSelected);
						}
					}
					lockerSelected.clear();
					std::cout << "clear\n";

					if (match)
					{
						for (int i = grid::gridRow - 1; i >= 0; i--)
						{
							for (int j = 0; j < grid::gridCol; j++)
							{
								if (grid::locker[i][j] == NULL)
								{
									for (int k = i; k >= 0; k--)
									{
										if (grid::locker[k][j] != NULL)
										{
											grid::locker[k][j]->setDestPosY(grid::rec.y + (i * locker::lockerHeight));

											locker::Locker* aux = grid::locker[i][j];
											grid::locker[i][j] = grid::locker[k][j];
											grid::locker[k][j] = aux;

											break;
										}
									}
								}
							}
						}

						int counterI[grid::gridCol] = { 0 };
						for (int i = grid::gridRow - 1; i >= 0; i--)
						{
							for (int j = 0; j < grid::gridCol; j++)
							{
								if (grid::locker[i][j] == NULL)
								{
									locker::add(i, j, counterI[j]);
									counterI[j]++;
								}
							}
						}

						if (!locker::checkLockerAvaibles())
						{
							locker::reset();
						}
					}
				}
			}
		}

		void Player::makeSkill(locker::LOCKER_TYPE lockerType, int quantity)
		{
			switch (lockerType)
			{
			case locker::LOCKER_TYPE::LIFE_POTION:
				setLife(life + (LIFE_POTION_VALUE * quantity));
				break;
			case locker::LOCKER_TYPE::MANA_POTION:
				setMana(mana + (MANA_POTION_VALUE * quantity));
				break;
			case locker::LOCKER_TYPE::FIRE_BALL:
				spell::add(spell::SPELL_TYPE::FIRE_BALL, quantity);
				break;
			case locker::LOCKER_TYPE::ICE_BALL:
				spell::add(spell::SPELL_TYPE::ICE_BALL, quantity);
				break;
			case locker::LOCKER_TYPE::THUNDER:
				spell::add(spell::SPELL_TYPE::THUNDER, quantity);
				break;
			case locker::LOCKER_TYPE::SHIELD:
				spell::add(spell::SPELL_TYPE::SHIELD, quantity);
				break;
			case locker::LOCKER_TYPE::RESTORATION:
				setLife(PLAYER_LIFE);
				setMana(PLAYER_MANA);
				break;
			default:
				break;
			}
		}

		void Player::setAnimation(character::ANIMATION_STATE animationState)
		{
			Texture2D auxTexture = Texture2D();
			Rectangle auxPlayerRec = { 0.0f, 0.0f, this->rec.width, this->rec.height };
			int auxFramesX = 0;
			int auxFramesY = 0;
			float auxSpeed = 0.0f;
			bool auxIsLoop = false;

			switch (animationState)
			{
			case character::ANIMATION_STATE::IDLE:
				auxPlayerRec.width = recWidth;
				auxPlayerRec.height = recHeight;

				auxFramesX = 6;
				auxFramesY = 2;

				auxTexture = texture::playerIdle;

				auxSpeed = 600.0f;
				auxIsLoop = true;

				break;
			case character::ANIMATION_STATE::HURT:
				auxPlayerRec.width = recWidth;
				auxPlayerRec.height = recHeight;

				auxFramesX = 6;
				auxFramesY = 2;

				auxTexture = texture::playerHurt;

				auxSpeed = 600.0f;
				auxIsLoop = false;

				break;
			case character::ANIMATION_STATE::ATTACK:
				auxPlayerRec.width = recWidth;
				auxPlayerRec.height = recHeight;

				auxFramesX = 6;
				auxFramesY = 2;

				auxTexture = texture::playerAttack;

				auxSpeed = 1200.0f;
				auxIsLoop = false;

				break;
			case character::ANIMATION_STATE::DEAD:
				auxPlayerRec.width = recWidth * 125 / 100;
				auxPlayerRec.height = recHeight * 125 / 100;

				auxFramesX = 6;
				auxFramesY = 3;

				auxTexture = texture::playerDead;

				auxSpeed = 600.0f;
				auxIsLoop = false;

				break;
			default:
				break;
			}

			auxTexture.width = static_cast<int>(auxPlayerRec.width * auxFramesX);
			auxTexture.height = static_cast<int>(auxPlayerRec.height * auxFramesY);

			this->animation = animation::Animation(auxTexture, auxPlayerRec, (auxFramesX * auxFramesY), auxSpeed, auxIsLoop);
		}

		bar::Bar Player::getManaBar()
		{
			return this->manaBar;
		}

		void Player::setManaBarForRec(Rectangle rec)
		{
			rec.height = bar::barHeight;
			rec.y -= rec.height * 2;
			this->manaBar = bar::Bar(rec, rec, bar::manaBarColor);
		}

		int Player::getMana()
		{
			return this->mana;
		}

		void Player::setMana(int mana)
		{
			if (!isDead)
			{
				if (mana <= 0)
				{
					this->mana = 0;
				}
				else if (mana > PLAYER_MANA)
				{
					this->mana = PLAYER_MANA;
				}
				else
				{
					this->mana = mana;
				}

				manaBar.update(this->mana, PLAYER_MANA);
			}
		}

		int Player::getScore()
		{
			return this->score;
		}

		void Player::setScore(int score)
		{
			this->score = score;
		}

		bool Player::getSelected()
		{
			return this->selected;
		}

		void Player::setSelected(bool selected)
		{
			this->selected = selected;
		}

		bool Player::getIsShield()
		{
			return this->isShield;
		}

		void Player::setIsShield(bool isShield)
		{
			this->isShield = isShield;
		}
	}
}