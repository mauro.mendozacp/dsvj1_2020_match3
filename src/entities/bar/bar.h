#ifndef BAR_H
#define BAR_H

#include "raylib.h"

namespace match3
{
	namespace bar
	{
		const Color lifeBarColor = RED;
		const Color manaBarColor = BLUE;
		const float barHeight = 5.0f;

		class Bar
		{
		public:
			Bar();
			Bar(Rectangle rec, Rectangle maxRec, Color color);
			~Bar();

			bool collision(Rectangle rec);
			void update(int value, int maxValue);
			void show();

			Rectangle getRec();
			void setRec(Rectangle rec);
			void setPositionX(float positionX);
			Rectangle getMaxRec();
			void setMaxRec(Rectangle maxRec);
			Color getColor();
			void setColor(Color color);

		private:
			Rectangle rec;
			Rectangle maxRec;
			Color color;
		};
	}
}

#endif // !BAR_H