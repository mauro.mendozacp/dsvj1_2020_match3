#include "info.h"
#include "game/game.h"

namespace match3
{
	namespace info
	{
		Vector2 rec[2];
		Vector2 timePos;
		Vector2 scorePos;
		Vector2 lifePlayer;
		Vector2 manaPlayer;
		Vector2 playerPos;
		Texture2D playerTexture;
		Texture2D background;

		void init()
		{
			rec[0].x = 0;
			rec[0].y = static_cast<float>(game::screenHeight / 2);

			rec[1].x = static_cast<float>(game::screenWidth * 3 / 4);
			rec[1].y = static_cast<float>(game::screenHeight / 2);

			background = texture::infoBackground;
			background.width = game::screenWidth / 4;
			background.height = game::screenHeight / 2;

			timePos.x = rec[1].x + static_cast<float>(background.width / 2);
			timePos.y = rec[1].y + static_cast<float>(background.height * 3 / 8);

			scorePos.x = rec[1].x + static_cast<float>(background.width / 2);
			scorePos.y = rec[1].y + static_cast<float>(background.height * 5 / 8);

			playerTexture = texture::playerIdle;
			playerTexture.width = (background.width / 2) * 6;
			playerTexture.height = (background.height * 3 / 8) * 2;

			playerPos.x = static_cast<float>(background.width / 4);
			playerPos.y = rec[0].y + static_cast<float>(background.height / 2);

			lifePlayer.x = static_cast<float>(background.width / 4);
			lifePlayer.y = rec[0].y + static_cast<float>(background.height * 3 / 8);

			manaPlayer.x = static_cast<float>(background.width / 4);
			manaPlayer.y = rec[0].y + static_cast<float>(background.height * 2 / 8);
		}

		void draw()
		{
			DrawTextureEx(background, rec[0], 0.0f, 1.0f, WHITE);
			DrawTextureEx(background, rec[1], 0.0f, 1.0f, WHITE);

			Font auxFont = font::option;
			float auxFontSize = font::optionFontSize;
			float auxFontSpacing = font::optionSpacing;
			Color auxColor = WHITE;
			Vector2 auxPos;
			std::string auxText;

			int min = static_cast<int>(gameplay::timer) / 60;
			int seg = static_cast<int>(gameplay::timer) % 60;

			if (min < 10)
			{
				if (seg < 10)
				{
					auxText = FormatText("TIME: 0%i:0%i", min, seg);
				}
				else
				{
					auxText = FormatText("TIME: 0%i:%i", min, seg);
				}
			}
			else
			{
				if (seg < 10)
				{
					auxText = FormatText("TIME: %i:0%i", min, seg);
				}
				else
				{
					auxText = FormatText("TIME: %i:%i", min, seg);
				}
			}

			auxPos.x = (timePos.x - (MeasureTextEx(auxFont, &auxText[0], auxFontSize, auxFontSpacing).x / 2));
			auxPos.y = (timePos.y - (MeasureTextEx(auxFont, &auxText[0], auxFontSize, auxFontSpacing).y / 2));
			DrawTextEx(auxFont, &auxText[0], auxPos, auxFontSize, auxFontSpacing, auxColor);

			auxText = FormatText("SCORE: %i", gameplay::player->getScore());
			auxPos.x = (scorePos.x - (MeasureTextEx(auxFont, &auxText[0], auxFontSize, auxFontSpacing).x / 2));
			auxPos.y = (scorePos.y - (MeasureTextEx(auxFont, &auxText[0], auxFontSize, auxFontSpacing).y / 2));
			DrawTextEx(auxFont, &auxText[0], auxPos, auxFontSize, auxFontSpacing, auxColor);

			DrawTextureRec(playerTexture, { 0.0f, 0.0f, static_cast<float>(background.width / 2), static_cast<float>(background.height * 3 / 8) }, playerPos, WHITE);

			auxFontSize /= 2;
			auxFontSpacing /= 2;

			if (gameplay::player->getLife() < PLAYER_LIFE * 25 / 100)
			{
				auxColor = RED;
			}
			else
			{
				auxColor = WHITE;
			}

			auxText = FormatText("LIFE: %i/%i", gameplay::player->getLife(), PLAYER_LIFE);
			DrawTextEx(auxFont, &auxText[0], lifePlayer, auxFontSize, auxFontSpacing, auxColor);

			if (gameplay::player->getMana() < PLAYER_MANA * 25 / 100)
			{
				auxColor = RED;
			}
			else
			{
				auxColor = WHITE;
			}

			auxText = FormatText("MANA: %i/%i", gameplay::player->getMana(), PLAYER_MANA);
			DrawTextEx(auxFont, &auxText[0], manaPlayer, auxFontSize, auxFontSpacing, auxColor);
		}
	}
}