#include "option.h"
#include "game/game.h"

namespace match3
{
	namespace option
	{
		float titleRecWidth = 600.0f;
		float titleRecHeight = 150.0f;

		float optionRecWidth = 300.0f;
		float optionRecHeight = 50.0f;

		float pauseRecWidth = 50.0f;
		float pauseRecHeight = 50.0f;

		float highscoreRecWidth = 450.0f;
		float highscoreRecHeight = 70.0f;

		void init(Option* &title, std::string titleText, Option* options[], int optionLenght, std::string optionTexts[])
		{
			//Title
			Vector2 pos = { static_cast<float>(game::screenWidth / 2) - (titleRecWidth / 2),
				static_cast<float>(game::screenHeight / 4) - (titleRecHeight / 2) };

			Rectangle bgTexture = { 0, 0, titleRecWidth, titleRecHeight };

			Texture2D texture = texture::titleBanner;
			texture.width = static_cast<int>(titleRecWidth);
			texture.height = static_cast<int>(titleRecHeight);

			title = new option::Option(pos, bgTexture, texture, titleText, font::title, font::titleFontSize, font::titleSpacing, font::titleColor);

			//Options
			pos = { static_cast<float>(game::screenWidth / 2) - (optionRecWidth / 2),
				static_cast<float>(game::screenHeight / 2) - (optionRecHeight / 2) };

			bgTexture = { 0, 0, optionRecWidth, optionRecHeight };

			texture = texture::optionButton;
			texture.width = static_cast<int>(optionRecWidth);
			texture.height = static_cast<int>(optionRecHeight * 2);

			for (int i = 0; i < optionLenght; i++)
			{
				options[i] = new option::Option(pos, bgTexture, texture, optionTexts[i], font::option, 
					font::optionFontSize, font::optionSpacing, font::optionColorNoSelected);

				pos.y += optionRecHeight * 3 / 2;
			}
		}

		void highscoreInit(Option*& highscore, std::string titleText)
		{
			Vector2 pos = { static_cast<float>(game::screenWidth / 2) - (highscoreRecWidth / 2),
				static_cast<float>(game::screenHeight / 4) + highscoreRecHeight };

			Rectangle bgTexture = { 0, 0, highscoreRecWidth, highscoreRecHeight };

			Texture2D pauseTexture = texture::highscoreBackground;
			pauseTexture.width = static_cast<int>(highscoreRecWidth);
			pauseTexture.height = static_cast<int>(highscoreRecHeight);

			highscore = new option::Option(pos, bgTexture, pauseTexture, titleText, font::option, font::optionFontSize, font::optionSpacing, WHITE);
		}

		void pauseInit()
		{
			float pauseButtonspacing = 20.0f;
			Texture2D pauseTexture = texture::pauseButton;
			pauseTexture.width = static_cast<int>(pauseRecWidth);
			pauseTexture.height = static_cast<int>(pauseRecHeight * 2);

			gameplay::pause = new option::Option(
				{ game::screenWidth - pauseRecWidth - pauseButtonspacing, pauseButtonspacing },
				{ 0, 0, pauseRecWidth, pauseRecHeight }, pauseTexture, "", Font(), 0.0f, 0.0f, WHITE);
		}

		void update(Option* options[], int optionLenght)
		{
			for (int i = 0; i < optionLenght; i++)
			{
				options[i]->changeOptionState();
			}
		}

		int getOption(Option* options[], int optionLenght)
		{
			for (int i = 0; i < optionLenght; i++)
			{
				if (input::checkClickOnRec(options[i]->getRecPosition()))
				{
					return i + 1;
				}
			}

			return 0;
		}

		void draw(Option* title, Option* options[], int optionLenght)
		{
			title->show();

			for (int i = 0; i < optionLenght; i++)
			{
				options[i]->show();
			}
		}

		void highscoreDeInit(Option* &highscore)
		{
			if (highscore != NULL)
			{
				delete highscore;
				highscore = NULL;
			}
		}

		void pauseDeInit()
		{
			if (gameplay::pause != NULL)
			{
				delete gameplay::pause;
				gameplay::pause = NULL;
			}
		}

		void deInit(Option* &title, Option* options[], int optionLenght)
		{
			if (title != NULL)
			{
				delete title;
				title = NULL;
			}

			for (int i = 0; i < optionLenght; i++)
			{
				if (options[i] != NULL)
				{
					delete options[i];
					options[i] = NULL;
				}
			}
		}

		Option::Option()
		{
			this->position = Vector2();
			this->rec = Rectangle();
			this->texture = Texture2D();
			this->text = "vacio";
			this->font = Font();
			this->fontSize = 0.0f;
			this->spacing = 0.0f;
			this->color = Color();
		}

		Option::Option(Vector2 position, Rectangle rec, Texture2D texture, std::string text, Font font, float fontSize, float spacing, Color color)
		{
			this->position = position;
			this->rec = rec;
			this->texture = texture;
			this->text = text;
			this->font = font;
			this->fontSize = fontSize;
			this->spacing = spacing;
			this->color = color;
		}

		Option::~Option()
		{
		}

		void Option::show()
		{
			DrawTextureRec(texture, rec, position, color);

			Vector2 pos;
			pos.x = ((position.x + rec.width / 2) - (MeasureTextEx(font, &text[0], fontSize, spacing).x / 2));
			pos.y = ((position.y + rec.height / 2) - (MeasureTextEx(font, &text[0], fontSize, spacing).y / 2));

			DrawTextEx(font, &text[0], pos, fontSize, spacing, color);
		}

		void Option::changeOptionState()
		{
			if (input::checkMouseOnRec(getRecPosition()))
			{
				rec.y = rec.height;
			}
			else
			{
				rec.y = 0.0f;
			}
		}

		void Option::setRec(Rectangle rec)
		{
			this->rec = rec;
		}

		Rectangle Option::getRec()
		{
			return this->rec;
		}

		void Option::setPosition(Vector2 position)
		{
			this->position = position;
		}

		Vector2 Option::getPosition()
		{
			return this->position;
		}

		Rectangle Option::getRecPosition()
		{
			return { position.x, position.y, rec.width, rec.height };
		}

		void Option::setTexture(Texture2D texture)
		{
			this->texture = texture;
		}

		Texture2D Option::getTexture()
		{
			return this->texture;
		}

		void Option::setText(std::string text)
		{
			this->text = text;
		}

		std::string Option::getText()
		{
			return this->text;
		}

		void Option::setFont(Font font)
		{
			this->font = font;
		}

		Font Option::getFont()
		{
			return this->font;
		}

		void Option::setFontSize(float fontSize)
		{
			this->fontSize = fontSize;
		}

		float Option::setFontSize()
		{
			return this->fontSize;
		}

		void Option::setSpacing(float spacing)
		{
			this->spacing = spacing;
		}

		float Option::getSpacing()
		{
			return this->spacing;
		}

		void Option::setColor(Color color)
		{
			this->color = color;
		}

		Color Option::getColor()
		{
			return this->color;
		}
	}
}