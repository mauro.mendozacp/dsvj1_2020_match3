#ifndef WARRIOR_H
#define WARRIOR_H

#include "raylib.h"
#include "entities/enemy/enemy.h"

namespace match3
{
	namespace warrior
	{
		void init();
		void add();

		class Warrior : public enemy::Enemy
		{
		public:
			Warrior();
			Warrior(Rectangle rec, int life, int damage, float moveSpeed, float attackSpeed, float destinyX, int points, Sound attackSound);
			~Warrior();

			void setAnimation(character::ANIMATION_STATE animationState);

		private:

		};
	}
}

#endif // !WARRIOR_G