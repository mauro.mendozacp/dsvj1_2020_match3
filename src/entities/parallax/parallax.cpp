#include "parallax.h"
#include "scenes/gameplay/gameplay.h"

namespace match3
{
	namespace parallax
	{
		Parallax::Parallax()
		{
			this->rec = Rectangle();
			this->texture = Texture2D();
			this->speed = 0.0f;

			for (int i = 0; i < parallaxLenght; i++)
			{
				this->positionX[i] = 0.0f;
			}
		}

		Parallax::Parallax(Rectangle rec, Texture2D texture, float speed)
		{
			this->rec = rec;
			this->texture = texture;
			this->speed = speed;

			for (int i = 0; i < parallaxLenght; i++)
			{
				this->positionX[i] = this->rec.x + (this->rec.width * i);
			}
		}

		Parallax::~Parallax()
		{
		}

		void Parallax::move()
		{
			float posX = speed * gameplay::frameTime;

			for (int i = 0; i < parallaxLenght; i++)
			{
				positionX[i] -= posX;

				if (positionX[i] <= -rec.width)
				{
					positionX[i] = static_cast<float>(rec.width * (parallaxLenght - 1));
				}
			}
		}

		void Parallax::show()
		{
			for (int i = 0; i < parallaxLenght; i++)
			{
				DrawTextureEx(texture, { positionX[i], rec.y }, 0.0f, 1.0f, WHITE);
			}
		}

		Rectangle Parallax::getRec()
		{
			return this->rec;
		}

		void Parallax::setRec(Rectangle rec)
		{
			this->rec = rec;
		}

		Vector2 Parallax::getPosition()
		{
			return { this->rec.x, this->rec.y };
		}

		void Parallax::setPosition(Vector2 position)
		{
			this->rec.x = position.x;
			this->rec.y = position.y;
		}

		Texture2D Parallax::getTexture()
		{
			return this->texture;
		}

		void Parallax::setTexture(Texture2D texture)
		{
			this->texture = texture;
		}

		float Parallax::getSpeed()
		{
			return this->speed;
		}

		void Parallax::setSpeed(float cloudspeedSpeed)
		{
			this->speed = speed;
		}
	}
}