#ifndef CHARACTER_H
#define CHARACTER_H

#include "raylib.h"
#include "constants.h"
#include "entities/animation/animation.h"
#include "entities/bar/bar.h"

namespace match3
{
	namespace character
	{
		extern Color normalColor;
		extern Color hurtColor;
		extern Color freezeColor;

		enum class ANIMATION_STATE
		{
			IDLE,
			WALK,
			HURT,
			ATTACK,
			DEAD
		};

		class Character
		{
		public:
			Character();
			Character(Rectangle rec, int life, Color color);
			~Character();

			void updateAnimation();
			virtual void show();
			virtual void dead();
			void hurt(int damage);

			virtual void setAnimation(ANIMATION_STATE animationState) = 0;

			Rectangle getRec();
			void setRec(Rectangle rec);
			Vector2 getPosition();
			void setPosition(Vector2 position);
			ANIMATION_STATE getAnimationState();
			void setAnimationState(ANIMATION_STATE animationState);
			animation::Animation getAnimation();
			bar::Bar getLifeBar();
			void setLifeBarForRec(Rectangle rec);
			int getLife();
			void setLife(int life);
			int getMaxLife();
			void setMaxLife(int maxLife);
			Color getColor();
			void setColor(Color color);
			bool getIsAttack();
			void setIsAttack(bool isAttack);
			bool getIsHurt();
			void setIsHurt(bool isHurt);
			bool getIsDead();
			void setIsDead(bool isDead);

		protected:
			Rectangle rec;
			ANIMATION_STATE animationState;
			animation::Animation animation;
			bar::Bar lifeBar;
			int life;
			int maxLife;
			Color color;
			bool isAttack;
			bool isHurt;
			bool isDead;
		};
	}
}

#endif // !CHARACTER_H