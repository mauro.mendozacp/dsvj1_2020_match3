#ifndef TEXTURE_H
#define TEXTURE_H

#include "raylib.h"

namespace match3
{
	namespace texture
	{
		extern Texture2D warZoneBackground;
		extern Texture2D floor;
		extern Texture2D clouds;
		extern Texture2D pauseButton;
		extern Texture2D infoBackground;
		extern Texture2D countdown;

		extern Texture2D playerIdle;
		extern Texture2D playerHurt;
		extern Texture2D playerAttack;
		extern Texture2D playerDead;

		extern Texture2D fireBallSpell;
		extern Texture2D iceBallSpell;
		extern Texture2D thunderSpell;
		extern Texture2D shieldSpell;

		extern Texture2D enemyWarriorIdle;
		extern Texture2D enemyWarriorWalk;
		extern Texture2D enemyWarriorAttack;
		extern Texture2D enemyWarriorHurt;
		extern Texture2D enemyWarriorDead;

		extern Texture2D enemyGiantIdle;
		extern Texture2D enemyGiantWalk;
		extern Texture2D enemyGiantAttack;
		extern Texture2D enemyGiantHurt;
		extern Texture2D enemyGiantDead;

		extern Texture2D enemyArcherIdle;
		extern Texture2D enemyArcherWalk;
		extern Texture2D enemyArcherAttack;
		extern Texture2D enemyArcherHurt;
		extern Texture2D enemyArcherDead;

		extern Texture2D gridBackground;
		extern Texture2D lockerLifePotion;
		extern Texture2D lockerManaPotion;
		extern Texture2D lockerFireBall;
		extern Texture2D lockerIceBall;
		extern Texture2D lockerRestoration;
		extern Texture2D lockerThunder;
		extern Texture2D lockerShield;
		extern Texture2D lockerBackground;

		extern Texture2D menuBackground;
		extern Texture2D titleBanner;
		extern Texture2D optionButton;
		extern Texture2D highscoreBackground;
		extern Texture2D gameoverBackground;
		extern Texture2D selectDifficultyBackground;
		extern Texture2D creditsBackground;

		void init();
		void initGameplay();
		void deInit();
		void deInitGameplay();
	}
}

#endif // !TEXTURE_H