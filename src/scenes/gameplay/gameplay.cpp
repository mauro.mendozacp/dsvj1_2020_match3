#include "gameplay.h"
#include "game/game.h"

namespace match3
{
	namespace gameplay
	{
		option::Option* pause;
		player::Player* player;

		float frameTime = 0.0f;
		float timer = 0.0f;

		DIFFICULTY difficulty;

		bool startGame;

		static void pauseGame()
		{
			pause->changeOptionState();
			if (input::checkClickOnRec(pause->getRecPosition()))
			{
				pause::init();
				game::gameStatus = game::GAME_STATUS::PAUSE;
			}
		}

		static void gameover()
		{
			if (player->getIsDead())
			{
				if (player->getAnimation().getIsFinish())
				{
					gameover::init();
					game::gameStatus = game::GAME_STATUS::GAMEOVER;
				}
			}
		}

		void restartGame()
		{
			grid::deInit();
			warzone::deInit();
			player::deInit();
			option::pauseDeInit();

			frameTime = 0.0f;
			timer = 0.0f;

			startGame = false;

			grid::init();
			warzone::init();
			player::init();
			option::pauseInit();
			info::init();
		}

		void init()
		{
			texture::initGameplay();

			frameTime = 0.0f;
			timer = 0.0f;

			startGame = false;

			grid::init();
			warzone::init();
			player::init();
			option::pauseInit();
			info::init();
		}

		void update()
		{
			frameTime = GetFrameTime();

			if (startGame)
			{
				timer += frameTime;

				grid::update();
				warzone::update();
			}
			else
			{
				warzone::countdown.update();

				if (warzone::countdown.getIsFinish())
				{
					startGame = true;
					PlayMusicStream(audio::gameplayMusic);
				}
			}
			
			pauseGame();
			gameover();
		}

		void draw()
		{
			grid::draw();
			warzone::draw();
			info::draw();
			pause->show();
		}

		void deInit()
		{
			grid::deInit();
			warzone::deInit();
			player::deInit();
			option::pauseDeInit();
			texture::deInitGameplay();
		}
	}
}