#include "enemy.h"
#include "audio/audio.h"
#include "scenes/gameplay/gameplay.h"
#include "entities/warrior/warrior.h"
#include "entities/giant/giant.h"
#include "entities/archer/archer.h"

namespace match3
{
	namespace enemy
	{
		int enemyWarriorProb;
		int enemyGiantProb;
		int enemyArcherProb;

		void init()
		{
			warrior::init();
			giant::init();
			archer::init();

			switch (gameplay::difficulty)
			{
			case gameplay::DIFFICULTY::EASY:
				enemyWarriorProb = ENEMY_WARRIOR_RESP_EASY;
				enemyGiantProb = ENEMY_GIANT_RESP_EASY;
				enemyArcherProb = ENEMY_ARCHER_RESP_EASY;
				break;
			case gameplay::DIFFICULTY::MEDIUM:
				enemyWarriorProb = ENEMY_WARRIOR_RESP_MEDIUM;
				enemyGiantProb = ENEMY_GIANT_RESP_MEDIUM;
				enemyArcherProb = ENEMY_ARCHER_RESP_MEDIUM;
				break;
			case gameplay::DIFFICULTY::HARD:
				enemyWarriorProb = ENEMY_WARRIOR_RESP_HARD;
				enemyGiantProb = ENEMY_GIANT_RESP_HARD;
				enemyArcherProb = ENEMY_ARCHER_RESP_HARD;
				break;
			default:
				break;
			}
		}

		void add()
		{
			ENEMY_TYPE enemyType = ENEMY_TYPE::WARRIOR;

			int min = 1;
			int max = enemyArcherProb + enemyGiantProb + enemyWarriorProb;
			int prob = GetRandomValue(min, max);

			if (prob >= min && prob <= enemyArcherProb)
			{
				enemyType = ENEMY_TYPE::ARCHER;
			}
			else if (prob > enemyArcherProb && prob <= enemyArcherProb + enemyGiantProb)
			{
				enemyType = ENEMY_TYPE::GIANT;
			}
			else if (prob > enemyArcherProb + enemyGiantProb && prob <= max)
			{
				enemyType = ENEMY_TYPE::WARRIOR;
			}

			switch (enemyType)
			{
			case ENEMY_TYPE::WARRIOR:
				warrior::add();
				break;
			case ENEMY_TYPE::GIANT:
				giant::add();
				break;
			case ENEMY_TYPE::ARCHER:
				archer::add();
				break;
			default:
				break;
			}
		}

		void update()
		{
			int it = 0;
			for (enemy::Enemy* e : warzone::enemies)
			{
				if (e != NULL)
				{
					if (!e->getIsDead())
					{
						e->move();
						e->attack();
					}

					if (e->getIsDead() || !e->getIsFreeze())
					{
						e->updateAnimation();
					}

					if (e->getIsDead())
					{
						if (e->getAnimation().getIsFinish())
						{
							delete e;
							e = NULL;
							warzone::enemies.erase(warzone::enemies.begin() + it);
						}
					}
				}
				it++;
			}
		}

		void draw()
		{
			for (enemy::Enemy* e : warzone::enemies)
			{
				if (e != NULL)
				{
					e->show();
				}
			}
		}

		void deInit()
		{
			for (enemy::Enemy* e : warzone::enemies)
			{
				if (e != NULL)
				{
					delete e;
					e = NULL;
				}
			}
			warzone::enemies.clear();
		}

		Enemy::Enemy() : Character()
		{
			this->damage = 0;
			this->moveSpeed = 0.0f;
			this->attackSpeed = 0.0f;
			this->destinyX = 0.0f;
			this->animation = animation::Animation();
			this->attackTimer = 0.0f;
			this->points = 0;
			this->timer = 0.0f;
			this->isFreeze = false;
			this->attackSound = Sound();
		}

		Enemy::Enemy(Rectangle rec, int life, int damage, float moveSpeed, float attackSpeed, float destinyX, int points, Sound attackSound)
			: Character(rec, life, WHITE)
		{
			this->damage = damage;
			this->moveSpeed = moveSpeed;
			this->attackSpeed = attackSpeed;
			this->destinyX = destinyX;
			this->attackTimer = 0.0f;
			this->points = points;
			this->timer = 0.0f;
			this->isFreeze = false;
			this->attackSound = attackSound;
		}

		Enemy::~Enemy()
		{
		}

		void Enemy::freeze()
		{
			isFreeze = true;
			color = character::freezeColor;
		}

		void Enemy::dead()
		{
			gameplay::player->setScore(gameplay::player->getScore() + points);
			character::Character::dead();
		}

		void Enemy::attack()
		{
			if (isAttack)
			{
				if (animationState == character::ANIMATION_STATE::IDLE)
				{
					attackTimer += gameplay::frameTime;
				}

				if (attackTimer >= attackSpeed)
				{
					if (!gameplay::player->getIsDead() && !gameplay::player->getIsShield())
					{
						gameplay::player->hurt(damage);
						PlaySound(audio::hitPlayerSound);
					}

					attackTimer = 0;
					setAnimation(character::ANIMATION_STATE::ATTACK);
					PlaySound(attackSound);
				}

				if (animationState == character::ANIMATION_STATE::ATTACK && animation.getIsFinish())
				{
					setAnimation(character::ANIMATION_STATE::IDLE);
				}
			}
		}

		void Enemy::move()
		{
			if (!isDead)
			{
				if (!isHurt && !isFreeze)
				{
					if (rec.x > destinyX)
					{
						rec.x -= moveSpeed * gameplay::frameTime;
						lifeBar.setPositionX(rec.x);

						if (rec.x <= destinyX)
						{
							rec.x = destinyX;
							lifeBar.setPositionX(rec.x);
							isAttack = true;
							attackTimer = attackSpeed;
						}
					}
				}

				if (isHurt)
				{
					if (animation.getIsFinish())
					{
						isHurt = false;
						isAttack = false;
						color = character::normalColor;

						if (rec.x > destinyX)
						{
							setAnimation(character::ANIMATION_STATE::WALK);
						}
						else
						{
							setAnimation(character::ANIMATION_STATE::IDLE);
						}
					}
				}

				if (isFreeze)
				{
					timer += gameplay::frameTime;

					if (timer >= ICE_BALL_FREEZING)
					{
						isFreeze = false;
						timer = 0.0f;
						color = character::normalColor;
					}
				}
			}
		}

		int Enemy::getDamage()
		{
			return this->damage;
		}

		void Enemy::setDamage(int damage)
		{
			this->damage = damage;
		}

		float Enemy::getMoveSpeed()
		{
			return this->moveSpeed;
		}

		void Enemy::setMoveSpeed(float speed)
		{
			this->moveSpeed = speed;
		}

		float Enemy::getAttackSpeed()
		{
			return this->attackSpeed;
		}

		void Enemy::setAttackSpeed(float attackSpeed)
		{
			this->attackSpeed = attackSpeed;
		}

		float Enemy::getDestinyX()
		{
			return this->destinyX;
		}

		void Enemy::setDestintyX(float destinyX)
		{
			this->destinyX = destinyX;
		}

		float Enemy::getTimer()
		{
			return this->timer;
		}

		void Enemy::setTimer(float timer)
		{
			this->timer = timer;
		}

		bool Enemy::getIsFreeze()
		{
			return this->isFreeze;
		}

		void Enemy::setIsFreeze(bool isFreeze)
		{
			this->isFreeze = isFreeze;
		}

		Sound Enemy::getAttackSound()
		{
			return this->attackSound;
		}

		void Enemy::setAttackSound(Sound attackSound)
		{
			this->attackSound = attackSound;
		}
	}
}