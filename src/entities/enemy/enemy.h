#ifndef ENEMY_H
#define ENEMY_H

#include "raylib.h"
#include "entities/character/character.h"

namespace match3
{
	namespace enemy
	{
		enum class ENEMY_TYPE
		{
			WARRIOR = 1,
			GIANT,
			ARCHER
		};
		const int enemyTypesLenght = 3;

		void init();
		void add();
		void update();
		void draw();
		void deInit();

		class Enemy : public character::Character
		{
		public:
			Enemy();
			Enemy(Rectangle rec, int life, int damage, float moveSpeed, float attackSpeed, float destinyX, int points, Sound attackSound);
			~Enemy();

			void freeze();
			void dead();
			void attack();
			void move();

			virtual void setAnimation(character::ANIMATION_STATE animationState) = 0;

			int getDamage();
			void setDamage(int damage);
			float getMoveSpeed();
			void setMoveSpeed(float moveSpeed);
			float getAttackSpeed();
			void setAttackSpeed(float attackSpeed);
			float getDestinyX();
			void setDestintyX(float destinyX);
			float getTimer();
			void setTimer(float timer);
			bool getIsFreeze();
			void setIsFreeze(bool isFreeze);
			Sound getAttackSound();
			void setAttackSound(Sound attackSound);

		protected:
			int damage;
			float moveSpeed;
			float attackSpeed;
			float destinyX;
			float attackTimer;
			int points;
			float timer;
			bool isFreeze;
			Sound attackSound;
		};
	}
}

#endif // !ENEMY_H