#include "giant.h"
#include "audio/audio.h"
#include "texture/texture.h"
#include "scenes/gameplay/gameplay.h"

namespace match3
{
	namespace giant
	{
		Rectangle enemyGiantRec;

		void init()
		{
			enemyGiantRec.width = warzone::rec.width / 10;
			enemyGiantRec.height = warzone::rec.height * 3 / 8;
			enemyGiantRec.x = warzone::enemyStartPositionX;
			enemyGiantRec.y = warzone::floorY - enemyGiantRec.height;
		}

		void add()
		{
			float auxDestX = gameplay::player->getRec().x + gameplay::player->getRec().width;

			warzone::enemies.push_back(new Giant(
				enemyGiantRec, ENEMY_GIANT_LIFE, ENEMY_GIANT_DAMAGE, ENEMY_GIANT_MOVE_SPEED, 
				ENEMY_GIANT_ATTACK_SPEED, auxDestX, ENEMY_GIANT_POINTS, audio::swordSound));
		}

		Giant::Giant() : Enemy()
		{
		}

		Giant::Giant(Rectangle rec, int life, int damage, float moveSpeed, float attackSpeed, float destinyX, int points, Sound attackSound)
			: Enemy(rec, life, damage, moveSpeed, attackSpeed, destinyX, points, attackSound)
		{
			setAnimation(character::ANIMATION_STATE::WALK);
		}

		Giant::~Giant()
		{
		}

		void Giant::setAnimation(character::ANIMATION_STATE animationState)
		{
			Texture2D auxTexture = Texture2D();
			Rectangle auxPlayerRec = { 0.0f, 0.0f, this->rec.width, this->rec.height };
			int auxFramesX = 0;
			int auxFramesY = 0;
			float auxSpeed = 0.0f;
			bool auxIsLoop = false;

			switch (animationState)
			{
			case character::ANIMATION_STATE::IDLE:
				auxFramesX = 6;
				auxFramesY = 2;

				rec.x = destinyX;
				rec.y = enemyGiantRec.y;
				rec.width = enemyGiantRec.width;
				rec.height = enemyGiantRec.height;

				auxTexture = texture::enemyGiantIdle;
				auxPlayerRec.width = enemyGiantRec.width;
				auxPlayerRec.height = enemyGiantRec.height;

				auxSpeed = 600.0f;
				auxIsLoop = true;

				break;
			case character::ANIMATION_STATE::WALK:
				auxFramesX = 6;
				auxFramesY = 3;

				auxTexture = texture::enemyGiantWalk;
				auxPlayerRec.width = enemyGiantRec.width;
				auxPlayerRec.height = enemyGiantRec.height;

				auxSpeed = 600.0f;
				auxIsLoop = true;

				break;
			case character::ANIMATION_STATE::ATTACK:
				auxFramesX = 6;
				auxFramesY = 2;

				rec.x = destinyX - (auxPlayerRec.width * 25 / 100);
				rec.y = enemyGiantRec.y * 90 / 100;
				rec.width = enemyGiantRec.width * 125 / 100;
				rec.height = enemyGiantRec.height * 110 / 100;

				auxTexture = texture::enemyGiantAttack;
				auxPlayerRec.width = rec.width;
				auxPlayerRec.height = rec.height;

				auxSpeed = 900.0f;
				auxIsLoop = false;

				break;
			case character::ANIMATION_STATE::HURT:
				auxFramesX = 6;
				auxFramesY = 2;

				auxTexture = texture::enemyGiantHurt;
				auxPlayerRec.width = enemyGiantRec.width * 125 / 100;
				auxPlayerRec.height = enemyGiantRec.height;

				auxSpeed = 1200.0f;
				auxIsLoop = false;

				break;
			case character::ANIMATION_STATE::DEAD:
				auxFramesX = 6;
				auxFramesY = 3;

				auxTexture = texture::enemyGiantDead;
				auxPlayerRec.width = enemyGiantRec.width * 125 / 100;
				auxPlayerRec.height = enemyGiantRec.height;

				auxSpeed = 900.0f;
				auxIsLoop = false;

				break;
			default:
				break;
			}

			auxTexture.width = static_cast<int>(auxPlayerRec.width * auxFramesX);
			auxTexture.height = static_cast<int>(auxPlayerRec.height * auxFramesY);

			this->animationState = animationState;
			this->animation = animation::Animation(auxTexture, auxPlayerRec, (auxFramesX * auxFramesY), auxSpeed, auxIsLoop);
		}
	}
}