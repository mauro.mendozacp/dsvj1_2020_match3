#ifndef GAMEOVER_H
#define GAMEOVER_H

#include "raylib.h"

namespace match3
{
	namespace gameover
	{
		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif // !GAMEOVER_H