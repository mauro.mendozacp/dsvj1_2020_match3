#ifndef PLAYER_H
#define PLAYER_H

#include <vector>

#include "raylib.h"
#include "entities/lockerIndex/lockerIndex.h"
#include "entities/spell/spell.h"
#include "entities/character/character.h"
#include "entities/locker/locker.h"

namespace match3
{
	namespace player
	{
		void init();
		void deInit();

		class Player : public character::Character
		{
		public:
			Player();
			Player(Rectangle rec, int life, int mana);
			~Player();

			void attack();
			void update();
			void show();

			void clickOnFirstLocker();
			void selectLocker();
			void matchLocker();
			void makeSkill(locker::LOCKER_TYPE lockerType, int quantity);

			void setAnimation(character::ANIMATION_STATE animationState);

			bar::Bar getManaBar();
			void setManaBarForRec(Rectangle rec);
			int getMana();
			void setMana(int mana);
			int getScore();
			void setScore(int score);
			bool getSelected();
			void setSelected(bool selected);
			bool getIsShield();
			void setIsShield(bool isShield);

			std::vector<LockerIndex> lockerSelected;
			std::vector<spell::Spell*> spells;
		private:
			bar::Bar manaBar;
			int mana;
			int score;
			bool selected;
			bool isShield;
		};
	}
}

#endif // !PLAYER_H