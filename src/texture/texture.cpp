#include "texture.h"
#include "game/game.h"

namespace match3
{
	namespace texture
	{
		Texture2D warZoneBackground;
		Texture2D floor;
		Texture2D clouds;
		Texture2D pauseButton;
		Texture2D infoBackground;
		Texture2D countdown;

		Texture2D playerIdle;
		Texture2D playerHurt;
		Texture2D playerAttack;
		Texture2D playerDead;

		Texture2D fireBallSpell;
		Texture2D iceBallSpell;
		Texture2D thunderSpell;
		Texture2D shieldSpell;

		Texture2D enemyWarriorIdle;
		Texture2D enemyWarriorWalk;
		Texture2D enemyWarriorAttack;
		Texture2D enemyWarriorHurt;
		Texture2D enemyWarriorDead;

		Texture2D enemyGiantIdle;
		Texture2D enemyGiantWalk;
		Texture2D enemyGiantAttack;
		Texture2D enemyGiantHurt;
		Texture2D enemyGiantDead;

		Texture2D enemyArcherIdle;
		Texture2D enemyArcherWalk;
		Texture2D enemyArcherAttack;
		Texture2D enemyArcherHurt;
		Texture2D enemyArcherDead;

		Texture2D gridBackground;
		Texture2D lockerLifePotion;
		Texture2D lockerManaPotion;
		Texture2D lockerFireBall;
		Texture2D lockerIceBall;
		Texture2D lockerRestoration;
		Texture2D lockerThunder;
		Texture2D lockerShield;
		Texture2D lockerBackground;

		Texture2D menuBackground;
		Texture2D titleBanner;
		Texture2D optionButton;
		Texture2D highscoreBackground;
		Texture2D gameoverBackground;
		Texture2D selectDifficultyBackground;
		Texture2D creditsBackground;

		void init()
		{
			menuBackground = LoadTexture("res/assets/textures/menu_background.png");
			menuBackground.width = game::screenWidth;
			menuBackground.height = game::screenHeight;

			gameoverBackground = LoadTexture("res/assets/textures/gameover_background.png");
			gameoverBackground.width = game::screenWidth;
			gameoverBackground.height = game::screenHeight;

			selectDifficultyBackground = LoadTexture("res/assets/textures/select_difficulty_background.png");
			selectDifficultyBackground.width = game::screenWidth;
			selectDifficultyBackground.height = game::screenHeight;

			creditsBackground = LoadTexture("res/assets/textures/credits_background.png");
			creditsBackground.width = game::screenWidth;
			creditsBackground.height = game::screenHeight;

			titleBanner = LoadTexture("res/assets/textures/title_banner.png");
			optionButton = LoadTexture("res/assets/textures/option_button.png");
			highscoreBackground = LoadTexture("res/assets/textures/highscore_background.png");
		}

		void initGameplay()
		{
			warZoneBackground = LoadTexture("res/assets/textures/warzone_background.png");
			floor = LoadTexture("res/assets/textures/floor.png");
			clouds = LoadTexture("res/assets/textures/clouds.png");
			pauseButton = LoadTexture("res/assets/textures/pause_button.png");
			infoBackground = LoadTexture("res/assets/textures/info_background.png");
			countdown = LoadTexture("res/assets/textures/countdown.png");

			playerIdle = LoadTexture("res/assets/textures/player_idle.png");
			playerHurt = LoadTexture("res/assets/textures/player_hurt.png");
			playerAttack = LoadTexture("res/assets/textures/player_attack.png");
			playerDead = LoadTexture("res/assets/textures/player_dead.png");

			fireBallSpell = LoadTexture("res/assets/textures/fire_ball.png");
			iceBallSpell = LoadTexture("res/assets/textures/ice_ball.png");
			thunderSpell = LoadTexture("res/assets/textures/thunder.png");
			shieldSpell = LoadTexture("res/assets/textures/shield.png");

			enemyWarriorIdle = LoadTexture("res/assets/textures/enemy_warrior_idle.png");
			enemyWarriorWalk = LoadTexture("res/assets/textures/enemy_warrior_walk.png");
			enemyWarriorAttack = LoadTexture("res/assets/textures/enemy_warrior_attack.png");
			enemyWarriorHurt = LoadTexture("res/assets/textures/enemy_warrior_hurt.png");
			enemyWarriorDead = LoadTexture("res/assets/textures/enemy_warrior_dead.png");

			enemyGiantIdle = LoadTexture("res/assets/textures/enemy_giant_idle.png");
			enemyGiantWalk = LoadTexture("res/assets/textures/enemy_giant_walk.png");
			enemyGiantAttack = LoadTexture("res/assets/textures/enemy_giant_attack.png");
			enemyGiantHurt = LoadTexture("res/assets/textures/enemy_giant_hurt.png");
			enemyGiantDead = LoadTexture("res/assets/textures/enemy_giant_dead.png");

			enemyArcherIdle = LoadTexture("res/assets/textures/enemy_archer_idle.png");
			enemyArcherWalk = LoadTexture("res/assets/textures/enemy_archer_walk.png");
			enemyArcherAttack = LoadTexture("res/assets/textures/enemy_archer_attack.png");
			enemyArcherHurt = LoadTexture("res/assets/textures/enemy_archer_hurt.png");
			enemyArcherDead = LoadTexture("res/assets/textures/enemy_archer_dead.png");

			gridBackground = LoadTexture("res/assets/textures/grid_background.png");
			lockerLifePotion = LoadTexture("res/assets/textures/locker_life_potion.png");
			lockerManaPotion = LoadTexture("res/assets/textures/locker_mana_potion.png");
			lockerFireBall = LoadTexture("res/assets/textures/locker_fire_ball.png");
			lockerIceBall = LoadTexture("res/assets/textures/locker_ice_ball.png");
			lockerRestoration = LoadTexture("res/assets/textures/locker_restoration.png");
			lockerThunder = LoadTexture("res/assets/textures/locker_thunder.png");
			lockerShield = LoadTexture("res/assets/textures/locker_shield.png");
			lockerBackground = LoadTexture("res/assets/textures/locker_background.png");
		}

		void deInit()
		{
			UnloadTexture(menuBackground);
			UnloadTexture(titleBanner);
			UnloadTexture(optionButton);
			UnloadTexture(highscoreBackground);
			UnloadTexture(gameoverBackground);
			UnloadTexture(selectDifficultyBackground);
			UnloadTexture(creditsBackground);
		}

		void deInitGameplay()
		{
			UnloadTexture(warZoneBackground);
			UnloadTexture(floor);
			UnloadTexture(clouds);
			UnloadTexture(pauseButton);
			UnloadTexture(infoBackground);
			UnloadTexture(countdown);

			UnloadTexture(playerIdle);
			UnloadTexture(playerHurt);
			UnloadTexture(playerAttack);
			UnloadTexture(playerDead);

			UnloadTexture(fireBallSpell);
			UnloadTexture(iceBallSpell);
			UnloadTexture(thunderSpell);
			UnloadTexture(shieldSpell);

			UnloadTexture(enemyWarriorIdle);
			UnloadTexture(enemyWarriorWalk);
			UnloadTexture(enemyWarriorAttack);
			UnloadTexture(enemyWarriorHurt);
			UnloadTexture(enemyWarriorDead);

			UnloadTexture(enemyGiantIdle);
			UnloadTexture(enemyGiantWalk);
			UnloadTexture(enemyGiantAttack);
			UnloadTexture(enemyGiantHurt);
			UnloadTexture(enemyGiantDead);

			UnloadTexture(enemyArcherIdle);
			UnloadTexture(enemyArcherWalk);
			UnloadTexture(enemyArcherAttack);
			UnloadTexture(enemyArcherHurt);
			UnloadTexture(enemyArcherDead);

			UnloadTexture(gridBackground);
			UnloadTexture(lockerLifePotion);
			UnloadTexture(lockerManaPotion);
			UnloadTexture(lockerFireBall);
			UnloadTexture(lockerIceBall);
			UnloadTexture(lockerRestoration);
			UnloadTexture(lockerThunder);
			UnloadTexture(lockerShield);
			UnloadTexture(lockerBackground);
		}
	}
}