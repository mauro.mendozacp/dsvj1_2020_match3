#ifndef LOCKER_H
#define LOCKER_H

#include "raylib.h"
#include "entities/lockerIndex/lockerIndex.h"

namespace match3
{
	namespace locker
	{
		extern float lockerWidth;
		extern float lockerHeight;

		enum class LOCKER_TYPE
		{
			LIFE_POTION = 1,
			MANA_POTION,
			FIRE_BALL,
			ICE_BALL,
			THUNDER,
			SHIELD,
			RESTORATION
		};
		const int lockerTypesLenght = 7;

		bool checkLockerAvaibles();
		void reset();
		void add(int i, int j, int counterI);

		void init();
		void update();
		void draw();
		void deInit();

		class Locker
		{
		public:
			Locker();
			Locker(Rectangle rec, float destPositionY, Texture2D background, Color color, LOCKER_TYPE lockerType, float speed);
			~Locker();

			void move();
			void show();

			Rectangle getRec();
			void setRec(Rectangle rec);
			Vector2 getPos();
			void setPos(Vector2 pos);
			float getDestPosY();
			void setDestPosY(float destPositionY);
			Texture2D getBackground();
			void setBackground(Texture2D background);
			Texture2D getTexture();
			void setTexture(Texture2D texture);
			Color getColor();
			void setColor(Color color);
			LOCKER_TYPE getLockerType();
			void setLockerType(LOCKER_TYPE lockerType);
			bool getSelected();
			void setSelected(bool selected);
			float getSpeed();
			void setSpeed(float speed);

			Texture2D getTextureForLockerType();

		private:
			Rectangle rec;
			float destPositionY;
			Texture2D texture;
			Texture2D background;
			Color color;
			LOCKER_TYPE lockerType;
			bool selected;
			float speed;
		};
	}
}

#endif // !LOCKER_H